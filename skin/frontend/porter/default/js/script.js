/* Product Timer */
productTimer = {
	init: function(secondsDiff, id){
		daysHolder = jQuery('.timer-'+id+' .days span');
		hoursHolder = jQuery('.timer-'+id+' .hours span');
		minutesHolder = jQuery('.timer-'+id+' .minutes span');
		secondsHolder = jQuery('.timer-'+id+' .seconds span');
		var firstLoad = true;
		productTimer.timer(secondsDiff, daysHolder, hoursHolder, minutesHolder, secondsHolder, firstLoad);
		setTimeout(function(){
			jQuery('.timer-box').css('display', 'inline-block');
		}, 1100);
	},
	timer: function(secondsDiff, daysHolder, hoursHolder, minutesHolder, secondsHolder, firstLoad){
		setTimeout(function(){
			days = Math.floor(secondsDiff/86400);
			hours = Math.floor((secondsDiff/3600)%24);
			minutes = Math.floor((secondsDiff/60)%60);
			seconds = secondsDiff%60;
			secondsHolder.html(seconds);
			if(secondsHolder.text().length == 1){
				secondsHolder.html('0'+seconds);
			} else if (secondsHolder.text()[0] != 0) {
				secondsHolder.html(seconds);
			}
			if(firstLoad == true){
				daysHolder.html(days);
				hoursHolder.html(hours);
				minutesHolder.html(minutes);
				if(minutesHolder.text().length == 1){
					minutesHolder.html('0'+minutes);
				}
				if(hoursHolder.text().length == 1){
					hoursHolder.html('0'+hours);
				}
				if(daysHolder.text().length == 1){
					daysHolder.html('0'+days);
				}
				firstLoad = false;
			}
			if(seconds >= 59){
				if(minutesHolder.text().length == 1 || minutesHolder.text()[0] == 0 && minutesHolder.text() != 00){
					minutesHolder.html('0'+minutes);
				} else {
					minutesHolder.html(minutes);
				}
				if(hoursHolder.text().length == 1 || hoursHolder.text()[0] == 0 && hoursHolder.text() != 00){
					hoursHolder.html('0'+hours);
				} else {
					hoursHolder.html(hours);
				}
				if(daysHolder.text().length == 1 || daysHolder.text()[0] == 0 && daysHolder.text() != 00){
					daysHolder.html('0'+days);
				} else {
					daysHolder.html(days);
				}
			}

			secondsDiff--;
			productTimer.timer(secondsDiff, daysHolder, hoursHolder, minutesHolder, secondsHolder, firstLoad);
		}, 1000);
	}
}
/* Bootstrap vs Prototype compability fix */
jQuery.noConflict();
if (Prototype.BrowserFeatures.ElementExtensions) {
	var disablePrototypeJS = function (method, pluginsToDisable) {
		var handler = function (event) {
			event.target[method] = undefined;
			setTimeout(function () {
				delete event.target[method];
			}, 0);
		};
		pluginsToDisable.each(function (plugin) {
			jQuery(window).on(method + '.bs.' + plugin, handler);
		});
	},
	pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover', 'tab'];
	disablePrototypeJS('show', pluginsToDisable);
	disablePrototypeJS('hide', pluginsToDisable);
	}
	jQuery(document).ready(function ($) {
	$('.bs-example-tooltips').children().each(function () {
		$(this).tooltip();
	});
	$('.bs-example-popovers').children().each(function () {
		$(this).popover();
	});
});

/* Top Cart */
function topCartListener(e){
	var touch = e.touches[0];
	if(jQuery(touch.target).parents('.topCartContent').length == 0 && jQuery(touch.target).parents('.cart-button').length == 0 && !jQuery(touch.target).hasClass('cart-button')){
		jQuery('.top-cart .block-title').removeClass('active');
		jQuery('.topCartContent').slideUp(500).removeClass('active');
		document.removeEventListener('touchstart', topCartListener, false);

	}
}

function topCart(isOnHover){
	jQuery('header.header').each(function(){
		var thisHeader = jQuery(this);
		var blockTitle = jQuery('#header .top-cart .block-title');
		var subtotal = jQuery('#header .top-cart .title-cart .subtotal');
		var rightBlock = jQuery('#header .top-cart .title-cart .right-block');
		var subtotalWidth = jQuery(subtotal).outerWidth();
		var rightBlockWidth = jQuery(rightBlock).outerWidth();
		function standardMode(){
			thisHeader.find('.top-cart .block-title').off().on('click', function(event){
				event.stopPropagation();
				jQuery(this).toggleClass('active');
				if(jQuery(blockTitle).hasClass('active')) {
					jQuery(subtotal).animate({'left': blockTitle.outerWidth() - subtotalWidth}, 150);
					jQuery(rightBlock).animate({'right': blockTitle.outerWidth() - rightBlockWidth}, 150);
				} else {
					jQuery(subtotal).animate({'left': 0 }, 150);
					jQuery(rightBlock).animate({'right': 0 }, 150);
				}
				if(jQuery(this).parents('.top-cart').hasClass('slide') && jQuery(document.body).width() < 978){
					jQuery('.close-btn').on('click', function(){
						jQuery(this).parents('.top-cart').find('.block-title').removeClass('active');
						jQuery(subtotal).animate({'left': 0 }, 150);
						jQuery(rightBlock).animate({'right': 0 }, 150);
					});
				} else {
					jQuery(this).next('.topCartContent').slideToggle(500).toggleClass('active');
				}
				document.addEventListener('touchstart', topCartListener, false);
				jQuery(document).on('click.cartEvent', function(e) {
					if (jQuery(e.target).parents('.topCartContent').length == 0) {
						thisHeader.find('.top-cart .block-title').removeClass('active');
						thisHeader.find('.topCartContent').slideUp(500).removeClass('active');
						jQuery(subtotal).animate({'left': 0 }, 150);
						jQuery(rightBlock).animate({'right': 0 }, 150);
						jQuery(document).off('click.cartEvent');
					}
				});
			});
			thisHeader.find('.top-cart').off().on('mouseenter', function(event){
				event.stopPropagation();
				jQuery(this).find('.block-title').addClass('hover');
			});
			thisHeader.find('.top-cart').on('mouseleave', function(event){
				event.stopPropagation();
				jQuery(this).find('.block-title').removeClass('hover');
			});

		}
		if(isOnHover){
			if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/Android/i))){
				standardMode();
			}else{
				thisHeader.find('.top-cart').off().on('mouseenter mouseleave', function(event){
					event.stopPropagation();
					jQuery(this).find('.block-title').toggleClass('active');
					thisHeader.find('.topCartContent').stop().slideToggle(500).toggleClass('active');
				});
			}
		}else{
			standardMode();
		}
	});
}
/* Top Cart */

/* Labels height */
function labelsHeight(){
	jQuery('.label-type-1 .label-new, .label-type-1 .label-sale').each(function(){
		labelNewWidth = jQuery(this).outerWidth();
		if(jQuery(this).parents('.label-type-1').length){
			if(jQuery(this).hasClass('percentage')){
				lineHeight = labelNewWidth - labelNewWidth*0.22;
			}else{
				lineHeight = labelNewWidth;
			}
		} else{
			lineHeight = labelNewWidth;
		}
		jQuery(this).css({
			'height' : labelNewWidth,
			'line-height' : lineHeight + 'px'
		});
	});
}

/* Product Image */
function productImageSize(){
	if(jQuery('.product-image-zoom').length){
		productImage = jQuery('.product-image-zoom #image');
		productImage.parent().animate({'height': productImage.height()}, 100).parent().removeClass('loading');
		productImage.animate({'opacity': 1}, 100);
	}
}
/* Product Image */

/*Header Right Menu*/
function headerRightMenu(){
	if(jQuery('.header-menu').length){
		menuWrapper = jQuery('.header-menu');
		menuButton = jQuery('.right-menu-button');
		menuClose = jQuery('.header-menu .btn-close');
		menuNewWidth = (jQuery(document.body).width() - jQuery('.container').width())/2 + 20;
		menuStandartWidth = parseFloat(jQuery('.header-menu').css('min-width'));
		menuHeight = menuWrapper.children('.right-menu').outerHeight();
		if(menuNewWidth > menuStandartWidth) {
			menuWidth = menuNewWidth;
		} else {
			menuWidth = menuStandartWidth;
		}
		if(!jQuery('body').hasClass('rtl')){
			menuWrapper.css({
				'width' : menuWidth,
				'right' : -menuWidth,
				'height' : menuHeight
			});
		} else {
			menuWrapper.css({
				'width' : menuWidth,
				'left' : -menuWidth - 10,
				'height' : menuHeight
			});
		}
		menuButton.on('click', function(){
			jQuery(this).toggleClass('active');
			if(jQuery(this).hasClass('active')){
				menuWrapper.parent().css('position', 'static');
				setTimeout(function(){
					if(!jQuery('body').hasClass('rtl')){
						menuWrapper.css('right', 0);
					} else {
						menuWrapper.css('left', -10 + 'px');
					}
				}, 10);
			} else {
				setTimeout(function(){
					menuWrapper.parent().css('position', 'relative');
				}, 300);
				if(!jQuery('body').hasClass('rtl')){
					menuWrapper.css('right', -menuWidth);
				} else {
					menuWrapper.css('left', -menuWidth - 10);
				}
			}
		});
		menuClose.on('click', function(){
			menuButton.removeClass('active');
			if(!jQuery('body').hasClass('rtl')){
				menuWrapper.css('right', -menuWidth);
			} else {
				menuWrapper.css('left', -menuWidth - 10);
			}
			setTimeout(function(){
				menuWrapper.parent().css('position', 'relative');
			}, 300);
		});
		jQuery('div.right-menu > ul a').each(function(){
			if(jQuery(this).next('ul').length || jQuery(this).next('ul.submenu').length){
				jQuery(this).before('<span class="menu-item-button"><i class="fa fa-plus-square-o"></i><i class="fa fa-minus-square-o"></i></span>');
				jQuery(this).next('ul').slideUp('fast');
				jQuery(this).prev('.menu-item-button').on('click', function(){
					jQuery(this).toggleClass('active');
					jQuery(this).nextAll('ul, ul.submenu').slideToggle('medium');
				});
			}
		});
	}
}
/*Header Right Menu*/

/* Wide Menu Top */
function WideMenuTop() {
	if (jQuery(document.body).width() > 767) {
		setTimeout(function(){
			if(!jQuery('#header').hasClass('header-15')) {
				jQuery('.nav-wide li .menu-wrapper').each(function() {
					WideMenuItemHeight = jQuery(this).parent().height();
					WideMenuItemPos = jQuery(this).parent().position().top;
					jQuery(this).css('top', (WideMenuItemHeight + WideMenuItemPos));
				});
			} else {
				jQuery('.nav-wide li .menu-wrapper, ul.topmenu:not(.nav-wide) li.level-top > ul').each(function() {
					WideMenuItemPos = jQuery(this).parent().position().top;
					jQuery(this).css('top', WideMenuItemPos);
				});
			}
		}, 100)
	} else {
		jQuery('.nav-wide li .menu-wrapper').css('top', 'auto');
	}
}



/* Product listing images */
var imageList;
function imageChanger(){
	if(jQuery(document.body).width() < 768){
		imgScrAttr = 'data-src-mobile';
	}else{
		imgScrAttr = 'data-src-desktop';
	}
	imageList.each(function(){
		jQuery(this).attr('src', jQuery(this).attr(imgScrAttr));
	});
}

function imageController(restart){
	if(restart){
		imageList = jQuery('img[data-src-mobile]');
	}
	if(imageList.length){
		imageChanger();

		function run(){
			imageChanger();
		}

		if(jQuery(document.body).width() < 768){
			var orientation = 'mobile';
		}else{
			var orientation = 'base';
		}

		jQuery(window).off('resize.imgcontrol').on('resize.imgcontrol', function(){
			if(jQuery(document.body).width() < 768){
				if(orientation != 'mobile'){
					orientation = 'mobile';
					run();
				}
			}else{
				if(orientation != 'base'){
					orientation = 'base';
					run();
				}
			}
		});
	}
}
function WideVerticalMenu(){
	if(jQuery('#header .vertical-menu-wrapper.default-open').length){
		menuWrapper = jQuery('#header .vertical-menu-wrapper');
		leftSidebar = jQuery('.col-left.sidebar, .home-sidebar');
		jQuery('.vertical-menu-wrapper li.level1').each(function(){
			thisElem = jQuery(this);
			if(thisElem.children('ul.level1').length){
				thisElem.addClass('parent');
				thisElem.children('ul.level1').attr('style', '');
				if(thisElem.data('columns') == 1){
					thisElem.addClass('default-dropdown');
				} else {
					if(thisElem.data('menuBgpos')){
						menuBgpos = thisElem.data('menuBgpos');
						thisElem.children('ul.level1').attr('style', menuBgpos);
					}
					if(thisElem.data('menuBg')){
						menuBg = thisElem.data('menuBg');
						menuBg = 'url('+menuBg+')';
						thisElem.children('ul.level1').css('background-image', menuBg);
					}
					if(thisElem.data('columns')){
						columns = thisElem.data('columns');
						columnWidth = 100/columns;
						thisElem.find('ul.level1 li.level2').css('width', columnWidth + '%');
					}
				}
			}
		});
		if (jQuery(document.body).width() > 1007 && jQuery(document.body).width() <= 1332){
			if(jQuery('.parent-menu-item-button').length == 0){
				jQuery('li.vertical-parent a.vertical-parent').after('<span class="parent-menu-item-button"><i class="icon-plus"></i><i class="icon-minus"></i></span>');
			}
			jQuery('.parent-menu-item-button').off().on('click', function(){
				jQuery(this).toggleClass('active').next('.vertical-menu-wrapper').toggleClass('shown-sub');
			});
		} else {
			jQuery('.parent-menu-item-button').remove();
		}
		if(leftSidebar.length){
			if(jQuery(document.body).width() > 1332){
				sidebarPosition = menuWrapper.outerHeight() - 20;
				if(leftSidebar.hasClass('col-left')){
					leftSidebar.css('padding-top', sidebarPosition + 20);
				} else {
					leftSidebar.css('padding-top', sidebarPosition);
				}
				menuWrapper.show();
			} else {
				menuWrapper.attr('style', '');
				leftSidebar.attr('style', '');
			}
		} else {
			menuWrapper.attr('style', '');
		}
	}
}

jQuery(window).load(function() {

	/* jQuery toTop plugin */
	if(jQuery('body').hasClass('totop-button')){
		(function($){
			$.fn.UItoTop = function(options) {
				var defaults = {
					min: 200,
					inDelay:600,
					outDelay:400,
					containerID: 'toTop',
					containerHoverID: 'toTopHover',
					scrollSpeed: 1200,
					easingType: 'linear'
				};
				var settings = $.extend(defaults, options);
				var containerIDhash = '#' + settings.containerID;
				var containerHoverIDHash = '#'+settings.containerHoverID;
				$('body').append('<a href="#" id="'+settings.containerID+'"></a>');
				$(containerIDhash).hide().click(function(){
					$('html, body').animate({scrollTop:0}, settings.scrollSpeed, settings.easingType);
					$('#'+settings.containerHoverID, this).stop().animate({'opacity': 0 }, settings.inDelay, settings.easingType);
					return false;
				})
				.prepend('<span id="'+settings.containerHoverID+'"><i class="icon-arrow-up"></i></span>')
				$(window).scroll(function() {
					var sd = $(window).scrollTop();
					if(typeof document.body.style.maxHeight === "undefined") {
						$(containerIDhash).css({
							'position': 'absolute',
							'top': $(window).scrollTop() + $(window).height() - 50
						});
					}
					if ( sd > settings.min )
						$(containerIDhash).fadeIn(settings.inDelay);
					else
						$(containerIDhash).fadeOut(settings.Outdelay);
				});
			};
		})(jQuery);
		jQuery().UItoTop();
	}

	/* Header Customer Block */
	function headerCustomer() {
		if(jQuery('#header .customer-name').length){
			var custName = jQuery('#header .customer-name-wrapper');
			jQuery('#header .links').hide();
			jQuery('header#header .customer-name').removeClass('open');
			jQuery('header#header .customer-name + .links').slideUp(500);
			jQuery('header#header .links li').each(function(){
				jQuery(this).find('a').append('<span class="hover-divider" />');
			});
			function headerCustomerListener(e){
				var touch = e.touches[0];
				if(jQuery(touch.target).parents('header#header .customer-name + .links').length == 0 && !jQuery(touch.target).hasClass('customer-name') && !jQuery(touch.target).parents('.customer-name').length){
					jQuery('header#header .customer-name').removeClass('open');
					jQuery('header#header .customer-name + .links').slideUp(500);
					document.removeEventListener('touchstart', headerCustomerListener, false);
				}
			}
			custName.parent().off().on('mouseenter', function(event){
				event.stopPropagation();
				jQuery(this).children().addClass('hover');
			});
			custName.parent().on('mouseleave', function(event){
				event.stopPropagation();
				jQuery(this).children().removeClass('hover');
			});
			custName.off().on('click', function(event){
				event.stopPropagation();
				jQuery(this).toggleClass('open');
				var linksTop = custName.position().top + custName.outerHeight(true);
				jQuery('#header .links').slideToggle().css('top', linksTop);
				document.addEventListener('touchstart', headerCustomerListener, false);
				jQuery(document).on('click.headerCustomerEvent', function(e) {
					if (jQuery(e.target).parents('header#header ul.links').length == 0) {
						jQuery('header#header .customer-name').removeClass('open');
						jQuery('header#header .customer-name + .links').slideUp(500);
						jQuery(document).off('click.headerCustomerEvent');
					}
				});
			});
		}
	}

	if(jQuery('.twitter-timeline').length){
		jQuery('.twitter-timeline').contents().find('head').append('<style>body{color: #aaa} body .p-author .profile .p-name{color: #fff}</style>');
	}

	/* Mobile Devices */
	if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))){
		/* Mobile Devices Class */
		jQuery('body').addClass('mobile-device');
		var mobileDevice = true;
	}else if(!navigator.userAgent.match(/Android/i)){
		var mobileDevice = false;
	}

	/* Responsive */
	var responsiveflag = false;
	var topSelectFlag = false;
	var menu_type = jQuery('#nav').attr('class');

	var isMenuAnimation = false;
	jQuery('.header h2.logo a.logo, div.topmenu a, a.cartHeader, .search-mini-form .search-button').on('click', function(event){
		if(isMenuAnimation){
			event.preventDefault();
		}
	});

	function mobile_menu(mode){
		switch(mode)
		 {
		 case 'animate':
		   if(!jQuery('div.topmenu').hasClass('mobile')){
				jQuery("div.topmenu").addClass('mobile');
				jQuery('div.topmenu > ul').slideUp('fast');
				menuButton = jQuery('.menu-button');
				if(menuButton.length){
					menuButton.removeClass('active').children('.mobile-menu-button').removeClass('close');
					jQuery('body').animate({'margin-left': '0', 'margin-right': '0'}, 500);
					var isActiveMenu = false;
					var isTouch = false;
					if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/Android/i))){
						var isTouch = true;
					}
					function callEvent(event){
						event.stopPropagation();
						if(isActiveMenu == false && !isMenuAnimation){
							isMenuAnimation = true;
							menuButton.addClass('active').children('.mobile-menu-button').addClass('close');
							jQuery(this).addClass('active');
							menuWidth = jQuery('.header .topmenu.navbar-collapse').css('width');
							jQuery('body').animate({
								'margin-left': menuWidth,
								'margin-right': '-' + menuWidth
							}, 250);
							jQuery('div.topmenu').addClass('in');
							jQuery('div.topmenu > ul').slideDown('medium', function(){
								setTimeout(function(){
									isMenuAnimation = false;
								}, 1000);
							});
							isActiveMenu = true;
							if(isTouch){
								document.addEventListener('touchstart', mobMenuListener, false);
							}else{
								jQuery(document).on('click.mobMenuEvent', function(e){
									if(jQuery(e.target).parents('div.topmenu.mobile').length == 0){
										closeMenu();
										document.removeEventListener('touchstart', mobMenuListener, false);
										jQuery(document).off('click.mobMenuEvent');
									}
								});
							}
						}else if(!isMenuAnimation){
							closeMenu();
						}
					}

					if(!isTouch){
						menuButton.on('click.menu', function(event){
							callEvent(event);
						});
					}else{
						document.getElementsByClassName('menu-button')[0].addEventListener('touchstart', callEvent, false);
					}

					function closeMenu(eventSet){
						menuButton.removeClass('active').children('.mobile-menu-button').removeClass('close');
						jQuery('body').animate({'margin-left': '0', 'margin-right': '0'}, 500);
						isMenuAnimation = true;
						jQuery('div.topmenu').removeClass('in');
						jQuery('div.topmenu > ul').slideUp('medium', function(){
							isMenuAnimation = false;
							isActiveMenu = false;
						});
						document.removeEventListener('touchstart', mobMenuListener, false);
					}
					function mobMenuListener(e){
						var touch = e.touches[0];
						if(jQuery(touch.target).parents('div.topmenu.mobile').length == 0 && jQuery(touch.target).parents('.menu-button').length == 0 && !jQuery(touch.target).hasClass('menu-button')){
							closeMenu();
						}
					}
				}
			   jQuery('div.topmenu > ul a').each(function(){
					if(jQuery(this).next('ul').length || jQuery(this).next('div.menu-wrapper').length){
						jQuery(this).before('<span class="menu-item-button"><i class="icon-plus"></i><i class="icon-minus"></i></span>')
						jQuery(this).next('ul').slideUp('fast');
						jQuery(this).prev('.menu-item-button').on('click', function(){
							jQuery(this).toggleClass('active');
							jQuery(this).nextAll('ul, div.menu-wrapper').slideToggle('medium');
						});
					}
			   });
		   }
		   break;
		 default:
				jQuery('div.topmenu').removeClass('mobile');
				jQuery('div.topmenu > ul').attr('style', '');
				jQuery('.menu-button').off();
				jQuery('.menu-item-button').remove();
		 }
	}

	function backgroundWrapper(){
		if(jQuery('.background-wrapper').length){
			jQuery('.background-wrapper').each(function(){
				var thisBg = jQuery(this);
				if(jQuery(document.body).width() < 768){
					thisBg.attr('style', '');
					if(thisBg.parent().hasClass('text-banner') || thisBg.find('.text-banner').length){
						bgHeight = thisBg.parent().outerHeight();
						thisBg.parent().css('height', bgHeight - 2);
					}
					if(jQuery('body').hasClass('boxed-layout')){
						bodyWidth = thisBg.parents('.container').outerWidth();
						bgLeft = (bodyWidth - thisBg.parents('.container').width())/2;
					} else {
						bgLeft = thisBg.parent().offset().left;
						bodyWidth = jQuery(document.body).width();
					}
					if(thisBg.data('bgColor')){
						bgColor = thisBg.data('bgColor');
						thisBg.css('background-color', bgColor);
					}
					setTimeout(function(){
						thisBg.css({
							'position' : 'absolute',
							'left' : -bgLeft,
							'width' : bodyWidth
						}).parent().css('position', 'relative');
					}, 300);
				} else {
					thisBg.attr('style', '');
					if(jQuery('body').hasClass('boxed-layout')){
						bodyWidth = thisBg.parents('.container').outerWidth();
						bgLeft = (bodyWidth - thisBg.parents('.container').width())/2;
					} else {
						bgLeft = thisBg.parent().offset().left;
						bodyWidth = jQuery(document.body).width();
					}
					thisBg.css({
						'position' : 'absolute',
						'left' : -bgLeft,
						'width' : bodyWidth
					}).parent().css('position', 'relative');
					if(thisBg.data('bgColor')){
						bgColor = thisBg.data('bgColor');
						thisBg.css('background-color', bgColor);
					}
					if(thisBg.parent().hasClass('text-banner') || thisBg.find('.text-banner').length){
						bgHeight = thisBg.children().innerHeight();
						thisBg.parent().css('height', bgHeight - 2);
					}
				}
				if(thisBg.parent().hasClass('parallax-banners-wrapper')) {
						jQuery('.parallax-banners-wrapper').each(function(){
							block = jQuery(this).find('.text-banner');
							var wrapper = jQuery(this);
							var fullHeight = 0;
							var imgCount = block.size();
							var currentIndex = 0;
							block.each(function(){
								imgUrl = jQuery(this).css('background-image').replace(/url\(|\)|\"/ig, '');
								if(imgUrl.indexOf('none')==-1){
									img = new Image;
									img.src = imgUrl;
									img.setAttribute("name", jQuery(this).attr('id'));
									img.onload = function(){
										imgName = '#' + jQuery(this).attr('name');
										if(wrapper.data('fullscreen')){
											windowHeight = document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
											jQuery(imgName).css({
												'height' : windowHeight+'px',
												'background-size' : '100% 100%'
											});
											fullHeight += windowHeight;
										} else {
											jQuery(imgName).css('height', this.height+'px');
											jQuery(imgName).css('height', (this.height - 200)+'px');
											fullHeight += this.height - 200;
										}
										wrapper.css('height', fullHeight);
										currentIndex++;
										if(!jQuery('body').hasClass('mobile-device')){
											if(currentIndex == imgCount){
												if(jQuery(document.body).width() > 1278) {
													jQuery('#parallax-banner-1').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-2').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-3').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-4').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-5').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-6').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-7').parallax("60%", 0.7, false);
													jQuery('#parallax-banner-8').parallax("60%", 0.7, false);
													jQuery('#parallax-banner-9').parallax("60%", 0.7, false);
													jQuery('#parallax-banner-10').parallax("60%", 0.7, false);
												} else if(jQuery(document.body).width() > 977) {
													jQuery('#parallax-banner-1').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-2').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-3').parallax("60%", 0.9, false);
													jQuery('#parallax-banner-4').parallax("60%", 0.85, false);
													jQuery('#parallax-banner-5').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-6').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-7').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-8').parallax("60%", 0.9, false);
													jQuery('#parallax-banner-9').parallax("60%", 0.85, false);
													jQuery('#parallax-banner-10').parallax("60%", 0.8, false);
												} else if(jQuery(document.body).width() > 767) {
													jQuery('#parallax-banner-1').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-2').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-3').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-4').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-5').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-6').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-7').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-8').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-9').parallax("60%", 0.8, false);
													jQuery('#parallax-banner-10').parallax("60%", 0.8, false);
												} else {
													jQuery('#parallax-banner-1').parallax("30%", 0.5, true);
													jQuery('#parallax-banner-2').parallax("60%", 0.1, false);
													jQuery('#parallax-banner-3').parallax("60%", 0.1, false);
													jQuery('#parallax-banner-4').parallax("60%", 0.1, false);
													jQuery('#parallax-banner-5').parallax("60%", 0.1, false);
													jQuery('#parallax-banner-6').parallax("60%", 0.1, false);
													jQuery('#parallax-banner-7').parallax("60%", 0.1, false);
													jQuery('#parallax-banner-8').parallax("60%", 0.1, false);
													jQuery('#parallax-banner-9').parallax("60%", 0.1, false);
													jQuery('#parallax-banner-10').parallax("60%", 0.1, false);
												}
											}
										}
									}
								}
								bannerText = jQuery(this).find('.banner-content');
								if(bannerText.data('top')){
									bannerText.css('top', bannerText.data('top'));
								}
								if(bannerText.data('left')){
									if(!bannerText.data('right')){
										bannerText.css({
											'left': bannerText.data('left'),
											'right' : 'auto'
										});
									} else {
										bannerText.css('left', bannerText.data('left'));
									}
								}
								if(bannerText.data('right')){
									if(!bannerText.data('left')){
										bannerText.css({
											'right': bannerText.data('right'),
											'left' : 'auto'
										});
									} else {
										bannerText.css('right', bannerText.data('right'));
									}
								}
							});
						});
						jQuery(window).scroll(function() {
							jQuery('.parallax-banners-wrapper').each(function(){
								block = jQuery(this).find('.text-banner');
								block.each(function(){
									var imagePos = jQuery(this).offset().top;
									var topOfWindow = jQuery(window).scrollTop();
									if (imagePos < topOfWindow+600) {
										jQuery(this).addClass("slideup");
									} else {
										jQuery(this).removeClass("slideup");
									}
								});
							});
						});
						setTimeout(function(){
							jQuery('#parallax-loading').fadeOut(200);
						}, 1000);
					}
					thisBg.animate({'opacity': 1}, 200)
			});
		}
	}


	/* Product tabs */
 	if(jQuery('.product-tabs-widget').length){
		function productTabs(){
			jQuery('ul.product-tabs').off().on('click', 'li:not(.current)', function() {
				jQuery(this).addClass('current').siblings().removeClass('current')
				.parents('div.product-tabs-widget').find('div.product-tabs-box').eq(jQuery(this).index()).fadeIn(800).addClass('visible').siblings('div.product-tabs-box').hide().removeClass('visible');
				labelsHeight();
			});
			jQuery('.product-tabs-widget').each(function(){
				listHeight = jQuery(this).find('.product-tabs').outerHeight(true);
				if(jQuery(this).hasClass('top-buttons')){
					if(jQuery(this).find('.widget-title').length){
						if(jQuery(document.body).width() < 767){
							titleHeight =  jQuery(this).find('.widget-title').innerHeight();
							blockTopIndent = parseFloat(jQuery(this).css('padding-top'));
							jQuery(this).find('.product-tabs').css('top', titleHeight + blockTopIndent + listHeight/2 + 5)
						} else {
							jQuery(this).find('.product-tabs').attr('style', '');
						}
					}
					jQuery(this).css('padding-top', listHeight);
				} else {
					jQuery(this).css('padding-bottom', listHeight);
				}
			});
		}
		productTabs();
		jQuery(window).resize(function(){productTabs();});
	}

	function replacingClass () {
		if (window.innerWidth < 480) { //Mobile
			mobile_menu('animate');
		}
		if (window.innerWidth > 479 && window.innerWidth < 768) { //iPhone
			mobile_menu('animate');
		}
		if (window.innerWidth > 767 && window.innerWidth <= 1007){ //Tablet
			mobile_menu('animate');
		}
		if (window.innerWidth > 1007 && window.innerWidth <= 1374){ //Desktop
			mobile_menu('reset');
		}
		if (window.innerWidth > 1374){ //Extra Large
			mobile_menu('reset');
		}
	}

	function customHomeSlider() {
		slider = jQuery("#home-image-slider");
		navigation = slider.data('navigation');
		pagination = slider.data('pagination');
		items = slider.data('items');
		itemsMobile = slider.data('itemsMobile');
		stagePadding = slider.data('stagePadding');
		slideSpeed = slider.data('speed');
		navigation ? navigation = true : navigation = false;
		pagination ? pagination = true : pagination = false;
		items ? items = items : items = 1;
		itemsMobile ? itemsMobile = itemsMobile : itemsMobile = 1;
		stagePadding ? stagePadding = stagePadding : stagePadding = 0;
		// pagination ? pagination = true : pagination = false;
		slider.owlCarousel({
			items : items,
			responsive:{
				0:{
					items:itemsMobile
				},
				767:{
					items:(items > 1 ? items = 2 : items = 1),
					margin: 0,
					stagePadding: 0,
					loop: true,
					center: true,
				},
				1331:{
					items: slider.data('items'),
					margin: 20,
					stagePadding: stagePadding,
					loop: true,
					center: true,
				},
			},
			nav: navigation,
			navSpeed: slideSpeed,
			dots: pagination,
			dotsSpeed: 400,
			navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
		});
	}

	function customTextSlider(){
		slider = jQuery("#custom-slider .owl-carousel");
		slider.owlCarousel({
			items : 1,
			nav: 0,
			dots: 1,
			dotsSpeed: 400
		});
	}

	headerCustomer();
	productImageSize();
	labelsHeight();
	replacingClass();
	backgroundWrapper();
	WideMenuTop();
	WideVerticalMenu();
	customHomeSlider();
	customTextSlider();
	headerRightMenu();
	jQuery(window).resize(function(){
		headerCustomer();
		productImageSize();
		labelsHeight();
		replacingClass();
		backgroundWrapper();
		WideMenuTop();
		WideVerticalMenu();
	});


	/* Mobile Devices */
	if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/Android/i))){
		var mobileDevice = true;
	}else{
		var mobileDevice = false;
	}

	/* Menu Wide */
	if(jQuery('.nav-wide').length){
		jQuery('.nav-wide li.level-top').mouseenter(function(){
			jQuery(this).addClass('over');
			if(mobileDevice == true){
				document.addEventListener('touchstart', wideMenuListener, false);
			}
		});
		jQuery('.nav-wide li.level-top').mouseleave(function(){
			jQuery(this).removeClass('over');
		});

		function wideMenuListener(e){
			var touch = e.touches[0];
			if(jQuery(touch.target).parents('div.menu-wrapper').length == 0){
				jQuery('.nav-wide li.level-top').removeClass('over');
				document.removeEventListener('touchstart', wideMenuListener, false);
			}
		}

		columnsWidth = function(columnsCount, currentGroupe){
            // HERFOX DISABLE TOP MENU FIX ITEMS MENU
			if(currentGroupe.size() > 1 && 0){
				currentGroupe.each(function(){
					jQuery(this).css('width', (100/currentGroupe.size())+'%');
				});
			}else{
				currentGroupe.css('width', (100/columnsCount)+'%');
			}
		}
		jQuery('.nav-wide .menu-wrapper').each(function(){
			columnsCount = jQuery(this).data('columns');
			items = jQuery(this).find('ul.level0 > li');
			groupsCount = items.size()/columnsCount;
			ratio = 1;
			for(i=0; i<groupsCount; i++){
				currentGroupe = items.slice((i*columnsCount), (columnsCount*ratio));
				/* set columns width */
				columnsWidth(columnsCount, currentGroupe);
				/* ==== */
				ratio++;
			}
		});

		/* Default Sub Menu in Wide Mode */
		elements = jQuery('.nav-wide .menu-wrapper.default-menu ul.level0 li');
		if(elements.length){
			elements.on('mouseenter mouseleave', function(){
				if(!jQuery('.nav-container').hasClass('mobile')){
					jQuery(this).children('ul').toggle();
				}
			});
			jQuery(window).resize(function(){
				if(!jQuery('.nav-container').hasClass('mobile')){
					elements.find('ul').hide();
				}
			});
			elements.each(function(){
				if(jQuery(this).children('ul').length){
					jQuery(this).addClass('parent');
				}
			});


			/* Default dropdown menu position */
			items = [];
			jQuery('.nav-wide li.level0').each(function(){
				if(jQuery(this).children('.default-menu').length){
					items.push(jQuery(this));
				}
			});
			jQuery(items).each(function(){
				jQuery(this).on('mouseenter mouseleave', function(){
					if(jQuery(this).hasClass('over')){
						if(!jQuery('body').hasClass('rtl')){
							jQuery(this).children('.default-menu').css({
								'top': jQuery(this).position().top,
								'left': jQuery(this).position().left
							});
						}else{
							jQuery(this).children('.default-menu').css({
								'top': jQuery(this).position().top,
								'left': jQuery(this).position().left - (jQuery(this).children('.default-menu').width() - jQuery(this).width())
							});
						}
					}else{
						jQuery(this).children('.default-menu').css('left', '-10000px');
					}
				});
			});
		}
	}
});

jQuery(document).ready(function(){
	/* Product listing images */
	imageList = jQuery('img[data-src-mobile]');
	imageController();

	/* Currency language block */
	jQuery('.language-currency-block').on('click', function(){
		jQuery('.language-currency-block').toggleClass('open');
		jQuery('.language-currency-dropdown').slideToggle();
	});

	var isApple = false;
	/* apple position fixed fix */
	if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))){
		isApple = true;

		function stickyPosition(clear){
			items = jQuery('header.header, .backstretch');
			if(clear == false){
				topIndent = jQuery(window).scrollTop();
				items.css({
					'position': 'absolute',
					'top': topIndent
				});
			}else{
				items.css({
					'position': 'fixed',
					'top': '0'
				});
			}
		}

		jQuery('.sticky-search header#sticky-header .form-search input').on('focusin focusout', function(){
			jQuery(this).toggleClass('focus');
			if(jQuery('header.header').hasClass('floating')){
				if(jQuery(this).hasClass('focus')){
					setTimeout(function(){
						stickyPosition(false);
					}, 500);
				}else{
					stickyPosition(true);
				}
			}
		});
	}

	/* sticky header */
	if(jQuery('#sticky-header').length){
		var headerHeight = jQuery('#header').height();
		sticky = jQuery('#sticky-header');
		jQuery(window).on('scroll', function(){
			if(jQuery(document.body).width() > 977){
				if(!isApple){
					heightParam = headerHeight;
				}else{
					heightParam = headerHeight*2;
				}
				if(jQuery(this).scrollTop() >= heightParam){
					sticky.slideDown(100);
					WideMenuTop();
				}
				if(jQuery(this).scrollTop() < headerHeight ){
					sticky.hide();
					WideMenuTop();
				}
			}
		});
	}

	function headerSearch(){
		jQuery('#header.header .search-button').each(function(){
			jQuery(this).off().on('click', function(){
				jQuery(this).parents('.form-search')
				.addClass('active')
				.find('.indent').append('<span class="btn-close" />').css({'left': 0, 'z-index': '999991'}).animate({
					'opacity' : 1
				}, 150);
				if(jQuery(this).parents('.form-search').hasClass('active') && jQuery(this).parents('.form-search').find('.btn-close').length){
					jQuery(this).parents('.form-search').find('.btn-close').on('click', function(){
						jQuery(this).parents('.indent').css('left', '-100%').animate({
							'opacity' : 0,
							'z-index' : '-1'
						}, 150);
						jQuery(this).parents('.form-search')
							.removeClass('active')
							.find('.btn-close').remove();
					});
				}
			});
		});
		jQuery('#sticky-header .search-mini-form').each(function(){
			if(!jQuery(this).find('.indent').hasClass('visible')){
				jQuery(this).find('.indent').css({
					'display' : 'none'
				});
				jQuery('#sticky-header .search-button').off().on('click', function() {
					jQuery('#sticky-header').find('.indent').toggleClass('visible').slideToggle();
				});
			}
		});
	}
	headerSearch();


	/* Light Box */
 	jQuery(document).on('click', '*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', function(event) {
		event.preventDefault();
		jQuery(this).ekkoLightbox();
	});
	// navigateTo
	jQuery(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
		event.preventDefault();
		return jQuery(this).ekkoLightbox({
			onShown: function() {
				var a = this.modal_content.find('.modal-footer a');
				if(a.length > 0) {
					a.click(function(e) {
						e.preventDefault();
						this.navigateTo(2);
					}.bind(this));
				}
			}
		});
	});


	if(jQuery('.toolbar-bottom .pager .pages').length == 0 || jQuery('.products-list li.item.type-2').length){
		jQuery('.toolbar-bottom').addClass('no-border');
	}

	if(jQuery('.footer-links-button').length){
		jQuery('.footer-links-button').on('click', function(){
			jQuery(this).toggleClass('active').parent().find('ul').slideToggle(300);
		});
	}
	if(jQuery('.custom-block .mobile-button').length){

		jQuery('.custom-block .mobile-button').click(function(){
			if(jQuery('.custom-block .indent').hasClass('active')){
				jQuery(this).prev('.indent').removeClass('active').animate({
					'opacity' : 0,
					'z-index' : '-1',
					'height' : '0'
				});
			} else {
				jQuery(this).prev('.indent').addClass('active').animate({
					'opacity' : 1,
					'z-index' : '999',
					'height' : '100%'
				});
			}
		});

	}

	/* Run Top Menus */
	$$(".nav").each(function(this_nav){
		new mainNav(this_nav, {"show_delay":"100","hide_delay":"100"});
	});

	if(jQuery('body').hasClass('header-with-image') && jQuery('.catalog-product-view .product-buttons').length){
		jQuery('.product-buttons').appendTo(jQuery('.breadcrumbs-inner'));
	}

	if(jQuery('.toolbar .pager .pages').length == 0){
		jQuery('.toolbar').addClass('no-pagination');
	}

	if (typeof(AjaxImageLoader) !== 'undefined') {
        AjaxImageLoader.individualStart = function(this_image)
        {
            this_image.closest('.product-image').addClass('loading');
        };

        AjaxImageLoader.individualSuccess = function(this_image)
        {
            this_image.closest('.product-image').removeClass('loading');
        };

		AjaxImageLoader.init();
	}


	if('undefined' != typeof AjaxKitMain) {
		 AjaxKitMain._reinitSubmodules = AjaxKitMain.reinitSubmodules;
		 AjaxKitMain.reinitSubmodules = function(){
			 AjaxKitMain._reinitSubmodules();
			 if('undefined' != typeof layeredNavigation) {
				layeredNavigation();
			}
		 }
	}
	if('undefined' != typeof GeneralToolbar) {
		GeneralToolbar.onLoadingStart = function(){
			jQuery('#toolbar-loading').show();
		}
		GeneralToolbar.onLoadingFinish = function(){
			jQuery('#toolbar-loading').hide();
			if('undefined' != typeof layeredNavigation) {
				layeredNavigation();
			}
		}
		GeneralToolbar.onInit = function(){
			jQuery('.selectpicker').selectpicker('refresh');
			imageController(true);
			if('undefined' != typeof ConfigurableSwatchesList) {
				ConfigurableSwatchesList.init();
			}
		}

		GeneralToolbar.onLoadingAutoScroll = function(){
			jQuery('#AjaxKit-InfiniteScroll').html(jQuery('.infinite-scroll-elements .infinite-scroll-loader'));
		}
		GeneralToolbar.onLoadingStaticScroll = function(){
			jQuery('#AjaxKit-InfiniteScroll').html(jQuery('.infinite-scroll-elements .infinite-scroll-loader'));
		}
		GeneralToolbar.onShowStaticScroll = function() {
			jQuery('#AjaxKit-InfiniteScroll').html(jQuery('.infinite-scroll-elements .infinite-scroll-button'));
		}




	} else {
		if('undefined' != typeof ConfigurableSwatchesList) {
			jQuery(document).on('configurable-media-images-init', function(){
				ConfigurableSwatchesList.init();
			});
		}
	}

	jQuery('.navbar-toggle').off().on('click', function(e){
		e.preventDefault();
		target = jQuery(this).data('target');
		jQuery(target).slideToggle('medium');
	});

	 jQuery('*[data-toggle="tooltip"]').on('mouseenter', function(){
		jQuery(this).tooltip('show');
	 });

	if(/iPhone|iPad|iPod|Mac/i.test(navigator.userAgent)){
		jQuery('body').addClass('apple-device');
	}

});



function appendFont(href)
{
    var link =document.createElement('link');
    link.href = href;
    link.type = 'text/css';
    link.rel = 'stylesheet';
    document.getElementsByTagName('head')[0].appendChild(link);
}
