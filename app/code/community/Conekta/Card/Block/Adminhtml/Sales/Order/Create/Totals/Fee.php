<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 4/26/17
 * Time: 5:42 PM
 *
 * @category   Conekta
 * @package    Conekta_Card_Fee
 * @author     Hernan Forigua (http://www.herfox.com)
 * @license    This work is free software, you can redistribute it and/or modify it
 */
class Conekta_Card_Block_Adminhtml_Sales_Order_Create_Totals_Fee extends Mage_Adminhtml_Block_Sales_Order_Create_Totals_Default
{
    /**
     * Use your own template if necessary
     * See "sales/order/create/totals/default.phtml" for model
     */
    // protected $_template = 'card/sales/order/create/totals/fee.phtml';
}