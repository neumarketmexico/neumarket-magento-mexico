<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 4/26/17
 * Time: 5:42 PM
 *
 * @category   Conekta
 * @package    Conekta_Card_Fee
 * @author     Hernan Forigua (http://www.herfox.com)
 * @license    This work is free software, you can redistribute it and/or modify it
 */

class Conekta_Card_Block_Sales_Order_Fee extends Mage_Core_Block_Template
{
    /**
     * Get order store object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }
    /**
     * Get totals source object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }
    /**
     * Initialize fee totals
     *
     * @return Conekta_Card_Block_Sales_Order_Fee
     */
    public function initTotals()
    {
        if ((float) $this->getOrder()->getBaseFeeAmount()) {
            $source = $this->getSource();
            $value  = $source->getFeeAmount();
            $this->getParentBlock()->addTotal(new Varien_Object(array(
                'code'   => 'card',
                'title'  => Mage::helper('card')->__('Costo Financiero'),
                'value'  => $value
            )));
        }
        return $this;
    }
}