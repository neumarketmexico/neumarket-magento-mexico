<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 4/26/17
 * Time: 5:47 PM
 *
 * @category   Conekta
 * @package    Conekta_Card_Fee
 * @author     Hernán Forigua (http://www.herfox.com)
 * @license    This work is free software, you can redistribute it and/or modify it
 */
class Conekta_Card_Block_Checkout_Totals_Fee extends Mage_Checkout_Block_Total_Default
{
    /**
     * Use your own template if necessary
     * See "checkout/total/default.phtml" for model
     */
    //protected $_template = 'card/checkout/total/fee.phtml';
}
