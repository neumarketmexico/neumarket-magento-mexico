<?php

/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 4/26/17
 * Time: 5:47 PM
 *
 * @category   Conekta
 * @package    Conekta_Card_Fee
 * @author     Hernán Forigua (http://www.herfox.com)
 * @license    This work is free software, you can redistribute it and/or modify it
 */
class Conekta_Card_AjaxController extends Mage_Checkout_Controller_Action
{
    public function indexAction()
    {

        $address = Mage::getModel("checkout/session")->getQuote()->getShippingAddress();
        $subtotal = $address->getSubtotalInclTax();
        $discount = $address->getDiscountAmount();
        $shipping = $address->getShippingInclTax();
        $shipping_desc = $address->getShippingDescription();
        $discount_desc = $address->getDiscountDescription();
        $total = $subtotal + $discount + $shipping;

        /*
        $address = Mage::getModel("checkout/session")->getQuote()->getShippingAddress();
        $subtotal = $this->getRequest()->getParam('subtotal');
        $discount = $this->getRequest()->getParam('discount');
        $shipping = $address->getShippingInclTax()*1;
        $total = $subtotal + $discount + $shipping;
        */
        $months = $this->getRequest()->getParam('months');

        //Mage::log($shipping, null, 'controller.log');
        //Mage::log($address->debug(), null, 'controller.log');

        $fee = Mage::getSingleton('card/fee')->calculateFee($months, $total);

        $grandtotal = $total;
        if ($months) $grandtotal += $fee['fee'];
        $result = array(
            'subtotal' => number_format($subtotal, 2),
            'fee' => $fee['fee'],
            'percent' => $fee['percent'],
            'grandtotal' => number_format($grandtotal,2),
            'shipping' => number_format($shipping, 2),
            'shipping_desc' => $shipping_desc,
            'discount' => number_format($discount,2),
            'discount_desc' => $discount_desc,
            'percent3' => $fee['percent3'],
            'percent6' => $fee['percent6'],
            'percent9' => $fee['percent9'],
            'percent12' => $fee['percent12']
        );

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
}