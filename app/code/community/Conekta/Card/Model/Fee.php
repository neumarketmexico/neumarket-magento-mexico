<?php

/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 4/26/17
 * Time: 5:47 PM
 *
 * @category   Conekta
 * @package    Conekta_Card_Fee
 * @author     Hernán Forigua (http://www.herfox.com)
 * @license    This work is free software, you can redistribute it and/or modify it
 */
class Conekta_Card_Model_Fee extends Varien_Object
{
    var $months = 0;
    var $subtotal = 0;
    var $discount = 0;
    var $shipping = 0;
    var $total = 0;

    public function calculateFee($months, $total)
    {

        $percent3 = Mage::getStoreConfig('payment/card/percent_installments_3');
        $percent6 = Mage::getStoreConfig('payment/card/percent_installments_6');
        $percent9 = Mage::getStoreConfig('payment/card/percent_installments_9');
        $percent12 = Mage::getStoreConfig('payment/card/percent_installments_12');

        switch ($months) {
            case 3:
                $percent = $percent3;
                break;
            case 6:
                $percent = $percent6;
                break;
            case 9:
                $percent = $percent9;
                break;
            case 12:
                $percent = $percent12;
                break;
            default:
                $percent = 0;
                break;
        }

        $fee = $total * $percent / 100;


        $active = 1;
        if($active) {
            // Promoción aplica a todos los productos
            $fee6 = number_format($total / 6, 2);
            if($months == 6) {
                $fee = 0;
                $byCategory = 43; // Promo Yokohama MSI to 3 Months = 192

                // Promoción aplica por categoría
                if ($byCategory) {
                    $fee6 = 0;
                    $promo = 0;
                    $cart = Mage::getModel('checkout/cart')->getQuote();

                    foreach ($cart->getAllItems() as $item) {
                        $product = $item->getProduct();
                        $qty = $item->getQty();
                        if ($product->getSpecialPrice()) {
                            $price = $product->getSpecialPrice();
                        } else {
                            $price = $product->getPrice();
                        }
                        $categoryIds = $product->getCategoryIds($product->getEntityId());
                        foreach ($categoryIds as $category) {
                            if ($category == $byCategory) {
                                $promo = 1;
                            }
                        }
                        $price = $price + (49 * $qty);
                        if ($promo) {
                            $fee6 += $price;
                        } else {
                            $fee6 += $price + ($price * $percent6 / 100);
                            $fee += ($price * $percent6 / 100);
                        }
                    }
                    $fee6 = number_format($fee6 / 6, 2);
                }
            }
        }
        else {
            $fee6 = number_format(($total+($total*$percent6/100))/6, 2);
        }
        // ---

        $fee3 = number_format(($total+($total*$percent3/100))/3, 2);
        $fee9 = number_format(($total+($total*$percent9/100))/9, 2);
        $fee12 = number_format(($total+($total*$percent12/100))/12, 2);


        return [
            "fee" => $fee,
            "percent" => $percent,
            "percent3" => $fee3,
            "percent6" => $fee6,
            "percent9" => $fee9,
            "percent12" => $fee12,
        ];
    }

    public function getFee()
    {
        $amount = $this->calculateFee($this->months, $this->total);
        return $amount['fee'];
    }

    /**
     * Check if fee can be apply
     *
     * @static
     * @param Mage_Sales_Model_Quote_Address $address
     * @return bool
     */
    public function canApply($address)
    {
        $method = $address->getQuote()->getPayment()->getMethod();
        $data = Mage::app()->getRequest()->getPost('payment', array());

        if ($method == "card" && $data) {
            if (isset($data['monthly_installments'])) {
                $this->months = $data['monthly_installments'];
                $this->subtotal = $address->getSubtotalInclTax();
                $this->discount = $address->getDiscountAmount();
                $this->shipping = $address->getShippingInclTax();
                $this->total = $this->subtotal + $this->discount + $this->shipping;

                return true;
            }
        }
        return false;
        // Put here your business logic to check if fee should be applied or not
        // Example of data retrieved :
        // $address->getShippingMethod(); > flatrate_flatrate
        // $address->getQuote()->getPayment()->getMethod(); > checkmo
        // $address->getCountryId(); > US
        // $address->getQuote()->getCouponCode(); > COUPONCODE
    }
}