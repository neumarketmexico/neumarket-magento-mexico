<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 4/26/17
 * Time: 5:42 PM
 *
 * @category   Conekta
 * @package    Conekta_Card_Fee
 * @author     Hernan Forigua (http://www.herfox.com)
 * @license    This work is free software, you can redistribute it and/or modify it
 */

class Conekta_Card_Model_Sales_Order_Total_Creditmemo_Fee extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
    /**
     * Collect credit memo total
     *
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @return Conekta_Card_Model_Sales_Order_Total_Creditmemo_Fee
     */
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        $order = $creditmemo->getOrder();
        $fee = $order->getFeeAmount();
        $baseFee = $order->getBaseFeeAmount();

        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $fee);
        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $baseFee);
        $creditmemo->setFeeAmount($fee);
        $creditmemo->setBaseFeeAmount($baseFee);
/*
        $fee = Mage::getSingleton('card/fee');
        if ($fee->canApply($creditmemo))
        {
            $amount = $fee->getFee();
        }
        if($order->getFeeAmountInvoiced() > 0) {
            $feeAmountLeft = $order->getFeeAmountInvoiced() - $order->getFeeAmountRefunded();
            $basefeeAmountLeft = $order->getBaseFeeAmountInvoiced() - $order->getBaseFeeAmountRefunded();
            if ($basefeeAmountLeft > 0) {
                $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $feeAmountLeft);
                $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $basefeeAmountLeft);
                $creditmemo->setFeeAmount($feeAmountLeft);
                $creditmemo->setBaseFeeAmount($basefeeAmountLeft);
            }
        } else {
            $feeAmount = $order->getFeeAmountInvoiced();
            $basefeeAmount = $order->getBaseFeeAmountInvoiced();
            $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $feeAmount);
            $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $basefeeAmount);
            $creditmemo->setFeeAmount($feeAmount);
            $creditmemo->setBaseFeeAmount($basefeeAmount);
        }
*/
        return $this;
    }
}