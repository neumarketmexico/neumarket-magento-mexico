<?php
/**
 * Created by PhpStorm.
 * User: Herfox
 * Date: 4/26/17
 * Time: 5:47 PM
 *
 * @category   Conekta
 * @package    Conekta_Card_Fee
 * @author     Hernán Forigua (http://www.herfox.com)
 * @license    This work is free software, you can redistribute it and/or modify it
 */
class Conekta_Card_Model_Sales_Quote_Address_Total_Fee extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    protected $_code = 'card';
    /**
     * Collect fee address amount
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Conekta_Card_Model_Sales_Quote_Address_Total_Fee
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);
        $this->_setAmount(0);
        $this->_setBaseAmount(0);
        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this;
        }
        $quote = $address->getQuote();
        $fee = Mage::getSingleton('card/fee');
        if ($fee->canApply($address))
        {
            $amount = $fee->getFee();
            $balance =  $amount - $quote->getFeeAmount();

            $address->setFeeAmount($balance);
            $address->setBaseFeeAmount($balance);

            $quote->setFeeAmount($balance);
            $address->setGrandTotal($address->getGrandTotal() + $address->getFeeAmount());
            $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseFeeAmount());
        }
        return $this;
    }
    /**
     * Add fee information to address
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Conekta_Card_Model_Sales_Quote_Address_Total_Fee
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $method = $address->getQuote()->getPayment();

        $fee = Mage::getSingleton('card/fee');
        if ($fee->canApply($address)) {
            $amount = $address->getFeeAmount();
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('card')->__('Costo Financiero'),
                'value' => $amount
            ));
        }
        return $this;
    }
}