<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials data helper
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Directory_Helper_Data extends Mage_Core_Helper_Abstract
{	
}