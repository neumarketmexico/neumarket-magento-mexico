<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials controller
 *
 * @category   No
 * @package    No_Serviteca
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Serviteca_indexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$this->loadLayout()
			->getLayout()->getBlock('head')
			->setTitle($this->__( 'Talleres Asociados' ) );
		
		$this->renderLayout();
    }
	
	public function getaddressbyservitecaidAction(){
		$data = $this->getRequest()->getPost();
		$response = null;
		
		if(isset($data['serviteca']) && $data['serviteca'] != ''){
			$serviteca = Mage::getModel('serviteca/serviteca')->load($data['serviteca']);
			if($serviteca->getData('is_active')){
				$response = $serviteca->getData();
			}
		}
		
		return $this->getResponse()->setBody( Mage::helper('core')->jsonEncode($response) );
	}
}