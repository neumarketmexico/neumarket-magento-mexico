<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials controller
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Serviteca_Adminhtml_ServitecaController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('serviteca/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Manager Serviteca'), Mage::helper('adminhtml')->__('Manager Serviteca'))
			->getLayout()->getBlock('head')->setTitle($this->__('Manager Serviteca'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('serviteca/adminhtml_serviteca'));
        $this->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('serviteca/serviteca')->load($id);
		
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);

			if (!empty($data)) {
				$model->setData($data);
			}
			
			Mage::register('serviteca_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('serviteca/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Serviteca Manager'), Mage::helper('adminhtml')->__('Serviteca Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Serviteca News'), Mage::helper('adminhtml')->__('Serviteca News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('serviteca/adminhtml_serviteca_edit'))
				->_addLeft($this->getLayout()->createBlock('serviteca/adminhtml_serviteca_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('serviteca')->__('Serviteca does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
				
	  			
			$model = Mage::getModel('serviteca/serviteca');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			try {
				$new_serviteca = $model->save();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('serviteca')->__('Serviteca was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('serviteca')->__('Unable to find size to serviteca'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				
				$model = Mage::getModel('serviteca/serviteca');
				
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Serviteca was successfully deleted'));
				
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $servitecaIds = $this->getRequest()->getParam('serviteca');
        if(!is_array($servitecaIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select serviteca(s)'));
        } else {
            try {
				
                foreach ($servitecaIds as $servitecaId) {
					$serviteca = Mage::getModel('serviteca/serviteca')->load($servitecaId);
                    $serviteca->delete();
                }
				Mage::getSingleton('adminhtml/session')->addSuccess(
					Mage::helper('serviteca')->__(
						'Total of %d record(s) were successfully deleted ', count($sizesIds)
					)
				);
				
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed()
    {
        return true;
    }

}