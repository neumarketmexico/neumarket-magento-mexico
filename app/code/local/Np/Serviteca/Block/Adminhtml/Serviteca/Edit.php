<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Serviteca_Block_Adminhtml_Serviteca_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'serviteca';
        $this->_controller = 'adminhtml_serviteca';
        
        $this->_updateButton('save', 'label', Mage::helper('serviteca')->__('Save Serviteca'));
        $this->_updateButton('delete', 'label', Mage::helper('serviteca')->__('Delete sServiteca'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('serviteca_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'serviteca_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'serviteca_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('serviteca_data') && Mage::registry('serviteca_data')->getId() ) {
            return Mage::helper('serviteca')->__("Edit Serviteca %s''", $this->htmlEscape(Mage::registry('serviteca_data')->getAncho().'/'.Mage::registry('serviteca_data')->getPerfil().' '.Mage::registry('serviteca_data')->getRin()));
        } else {
            return Mage::helper('serviteca')->__('Add Serviteca');
        }
    }
}