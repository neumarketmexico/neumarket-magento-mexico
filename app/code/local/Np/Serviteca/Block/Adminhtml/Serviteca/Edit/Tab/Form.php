<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Serviteca_Block_Adminhtml_Serviteca_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('serviteca_form', array('legend'=>Mage::helper('serviteca')->__('Serviteca information')));
     
      $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('cms')->__('Status'),
            'title'     => Mage::helper('cms')->__('Page Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array( '1' => Mage::helper('serviteca')->__('Enabled'), '0' => Mage::helper('serviteca')->__('Disabled'), ),
        ));
	  
	  $fieldset->addField('ciudad', 'text', array(
          'label'     => Mage::helper('serviteca')->__('Ciudad'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'ciudad',
      ));
	  
	  $fieldset->addField('nombre', 'text', array(
          'label'     => Mage::helper('serviteca')->__('Nombre'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'nombre',
      ));
	  
	  $fieldset->addField('direccion', 'text', array(
          'label'     => Mage::helper('serviteca')->__('Direccion'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'direccion',
      ));
	  
	  $fieldset->addField('valor', 'text', array(
          'label'     => Mage::helper('serviteca')->__('Valor montaje / cada llanta'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'valor',
      ));
	  
	  $fieldset->addField('tel', 'text', array(
          'label'     => Mage::helper('serviteca')->__('Telefono'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'tel',
      ));

      $fieldset->addField('lat', 'text', array(
          'label'     => Mage::helper('serviteca')->__('Latitud'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'lat',
      ));

      $fieldset->addField('lng', 'text', array(
          'label'     => Mage::helper('serviteca')->__('Longitud'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'lng',
      ));

      $fieldset->addField('descr', 'editor', array(
          'label'     => Mage::helper('serviteca')->__('Descripción'),
          'style'     => 'width:725px;height:360px',
          'required'  => false,
          'name'      => 'descr',
      ));
	  
      if ( Mage::getSingleton('adminhtml/session')->getServitecaData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getServitecaData());
          Mage::getSingleton('adminhtml/session')->setServitecaData(null);
      } elseif ( Mage::registry('serviteca_data') ) {
          $form->setValues(Mage::registry('serviteca_data')->getData());
      }
      return parent::_prepareForm();
  }
}