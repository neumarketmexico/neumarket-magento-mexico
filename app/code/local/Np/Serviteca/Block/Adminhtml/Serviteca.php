<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Serviteca_Block_Adminhtml_Serviteca extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_serviteca';
    $this->_blockGroup = 'serviteca';
    $this->_headerText = Mage::helper('serviteca')->__('Manage Serviteca');
    $this->_addButtonLabel = Mage::helper('serviteca')->__('Add Serviteca');
    parent::__construct();
  }
}