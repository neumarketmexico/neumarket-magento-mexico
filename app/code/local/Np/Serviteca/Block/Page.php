<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Np_Serviteca_Block_Page extends Mage_Core_Block_Template
{
	protected function _getServitecaModel()
	{
		return Mage::getModel('serviteca/serviteca');
	}
	
	protected function _getServitecaCollection()
	{
		return $this->_getServitecaModel()->getCollection();
	}
	
	public function getCiudades()
	{
		$collection = $this->_getServitecaCollection();
		$ciudades = array();
		
		if( count($collection) ){
			$ciudades[] = '';
		}
		
		foreach($collection as $serviteca){
			if( !in_array($serviteca->getCiudad(), $ciudades) ){
				$ciudades[] = $serviteca->getCiudad();
			}
		}
		
		return $ciudades;
	}
	
	public function getServitecaByCiudad($ciudad = NULL)
	{
		$collection = $this->_getServitecaCollection();
		$collection->addFieldToFilter('ciudad', $ciudad);
		$collection->addFieldToFilter('is_active',1);
		return $collection;
	}

    public function getGoogleMapsKey()
    {
        //API Key de Google Maps
        $key = 'AIzaSyCikl1WH6Q9vHLm2i9-llkZj8-fPmL8qgU';
        return $key;
    }
}