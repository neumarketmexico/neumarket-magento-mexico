<?php
/**
 * Hernán Forigua
 * herfox@live.com
 * @category    Herfox
 * @package     Servitecas_Neumarket
 * @copyright   Copyright (c) 2017 Hernán Forigua (http://www.herfox.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();
/* @var $addressHelper Mage_Customer_Helper_Address */
$addressHelper = Mage::helper('customer/address');
$store         = Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID);

/* @var $eavConfig Mage_Eav_Model_Config */
$eavConfig = Mage::getSingleton('eav/config');

// update customer address user defined attributes data
$attributes = array(
    'placa'           => array(
        'label'    => 'Placa del Vehículo',
        'backend_type'     => 'varchar',
        'frontend_input'    => 'text',
        'is_user_defined'   => 1,
        'is_system'         => 0,
        'is_visible'        => 1,
        'sort_order'        => 140,
        'is_required'       => false,
        'multiline_count'   => 0
    ),
);

foreach ($attributes as $attributeCode => $data) {
    $attribute = $eavConfig->getAttribute('customer_address', $attributeCode);
    $attribute->setWebsite($store->getWebsite());
    $attribute->addData($data);
    $usedInForms = array(
        'adminhtml_customer_address',
        'customer_address_edit',
        'customer_register_address'
    );
    $attribute->setData('used_in_forms', $usedInForms);
    $attribute->save();
}

$installer->run("
        ALTER TABLE {$this->getTable('sales_flat_quote_address')} ADD COLUMN `placa` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `fax`;
         ALTER TABLE {$this->getTable('sales_flat_order_address')} ADD COLUMN `placa` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `fax`;
        ");
$installer->endSetup();