<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$conn = $installer->getConnection();
$table = $installer->getTable('serviteca');

$conn->addColumn($table, 'valor', 'varchar(100)');

$installer->endSetup();