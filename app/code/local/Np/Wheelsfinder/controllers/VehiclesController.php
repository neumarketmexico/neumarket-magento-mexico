<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials controller
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_vehiclesController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
    {
		$url = Mage::app()->getStore()->getBaseUrl();
		Mage::app()->getResponse()->setRedirect( $url );
    }
	
	protected function _getVehiclesSession()
	{
		return Mage::getSingleton('core/session');
	}
	
	public function recoveryAction()
    {
		$data = $this->getRequest()->getPost();

		$marca = NULL;
		$modelo = NULL;
		$linea = NULL;
		
		if( isset($data['wf-marcas']) && !empty($data['wf-marcas']) ){
			$marca = $data['wf-marcas'];
		}

		if( isset($data['wf-modelos']) && !empty($data['wf-modelos']) ){
			$modelo = $data['wf-modelos'];
		}
		
		if($marca && !$modelo && !$linea){
			
			$this->_getVehiclesSession()->setData('wf_marca', $marca);
			Mage::registry('wf_marca', $marca);
			
			$modelos = Mage::getModel('wheelsfinder/vehicles')->getModelosByMarca($marca);
			
			$response = new Varien_Object();
			$response->setData('data_modelos', $modelos);
			
			return $this->getResponse()->setBody( Mage::helper('core')->jsonEncode($response) );
			
		}
		
		if(!$marca && $modelo && !$linea){
			
			$this->_getVehiclesSession()->setData('wf_modelo', $modelo);
			Mage::registry('wf_modelo', $modelo);
			
			$lineas = Mage::getModel('wheelsfinder/vehicles')->getLineasByMarcaAndModelo($this->_getVehiclesSession()->getData('wf_marca'), $modelo);
			
			$response = new Varien_Object();
			$response->setData('data_lineas', $lineas);
			
			return $this->getResponse()->setBody( Mage::helper('core')->jsonEncode($response) );
			
		}
		
    }
	
	public function resultAction()
    {
		$aliases = $this->getRequest()->getAliases();
		$redirect_rewrite = NULL;
		
		if(!count($aliases) && !isset($aliases['rewrite_request_path']) ){
			$redirect_rewrite = true;
		}

		$data = $this->getRequest()->getParams();
		$session = $this->_getVehiclesSession();

		$marca 	= NULL;
		$modelo = NULL;
		$linea 	= NULL;
		$title 	= NULL;
		$metatitle = NULL;
		$message = NULL;
		$criteria = array();

		if( isset($data['wf-marcas']) and !empty($data['wf-marcas']) ){
			$marca = $data['wf-marcas'];
			$session->setData('wf_marca', $marca);
			
			Mage::registry('wf_marca', $marca);
			
			$title = $title.$marca;
			$criteria['Marca'] = $marca;
		}
		
		if(isset( $data['wf-modelos']) and !empty($data['wf-modelos']) ){
			$modelo = $data['wf-modelos'];
			$session->setData('wf_modelo', $modelo);
			
			Mage::registry('wf_modelo', $modelo);
			
			$criteria['Modelo'] = $modelo;
		}
		
		if(isset($data['wf-lineas']) and !empty($data['wf-lineas']) ){
			
			$linea = $data['wf-lineas'];
			$session->setData('wf_linea', $linea);
			
			Mage::registry('wf_linea', $linea);
			
			$title = $title.' '.$linea;
			$title = $title.' '.$modelo;			
            $metatitle = $title.' | Neumarket Mexico';
			$criteria['L&iacute;nea'] = $linea;
		}
		
		$vehicle_id = Mage::getModel('wheelsfinder/vehicles')->getVehicleIdByFields($marca, $modelo, $linea);
		$vehicle = Mage::getModel('wheelsfinder/vehicles')->load($vehicle_id);
		
		if($vehicle_id){
			$vehicle = Mage::getModel('wheelsfinder/vehicles')->load($vehicle_id);
		}else{
			 return Mage::app()->getResponse()->setRedirect( Mage::helper('core/url')->getHomeUrl() ); 
			 
			  /*$response = Mage::app()->getResponse()
			 	->setHeader("Location", "/customer/account/login")
				->sendHeaders(); */
		}
		
		if($redirect_rewrite){
			if($vehicle->getUrlRewrite()){
                /* Modificación para formulario Dunlop */
                if(isset($data['fabricante'])){
                    if($data['fabricante']=='dunlop'){
                        Mage::app()->getResponse()->setRedirect($vehicle->getUrlRewrite().'?brands=50');
                    }elseif($data['fabricante']=='toyo'){
                        Mage::app()->getResponse()->setRedirect($vehicle->getUrlRewrite().'?brands=250');
                    }

                }
                else {
                    Mage::app()->getResponse()->setRedirect($vehicle->getUrlRewrite());
                }
			}
		}
		
		if( $vehicle->getData('mensaje') != '' && $vehicle->getData('mensaje') != NULL ){
			$message = $vehicle->getData('mensaje');
		}
		
		$sizes = Mage::getModel('wheelsfinder/vehicles')->getVehiclesSizesByVechicleId($vehicle_id);
		$sizes_clear = array();
		
		if( count($sizes) ){
		
			foreach($sizes as $size){
				
				if(!in_array($size, $sizes_clear)){
						$sizes_clear[] = $size;
				}
			}
		
		}
		
		
		$session
			->setData('size_results', $sizes_clear)
			->setData('criteria', $criteria)
			->setData('wf-message', $message);
			//->addSuccess($this->__('Resultados de busqueda por vehiculo: ').$title);

		$this->loadLayout()
			->getLayout()->getBlock('head')
			->setDescription($this->__('Compra tus llantas para ').$title.(' en Neumarket M��xico. Llama sin costo al 01800 8906213 desde cualquier lugar de la Rep��blica Mexicana.'))
			->setKeywords($this->__('llantas ').$title.(' neumarket colombia bogota'))			
			->setTitle($this->__( 'Llantas para ' ).$metatitle);
		
		$this->renderLayout();
		
    }
}