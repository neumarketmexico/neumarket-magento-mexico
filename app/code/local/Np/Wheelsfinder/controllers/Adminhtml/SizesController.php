<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials controller
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_Adminhtml_SizesController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('sizes/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Manager Sizes'), Mage::helper('adminhtml')->__('Manager Sizes'))
			->getLayout()->getBlock('head')->setTitle($this->__('Manager Sizes'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('wheelsfinder/adminhtml_sizes'));
        $this->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('wheelsfinder/sizes')->load($id);
		
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			
			if (!empty($data)) {
				$model->setData($data);
			}
			
			Mage::register('sizes_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('sizes/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Size Manager'), Mage::helper('adminhtml')->__('Size Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Size News'), Mage::helper('adminhtml')->__('Size News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('wheelsfinder/adminhtml_sizes_edit'))
				->_addLeft($this->getLayout()->createBlock('wheelsfinder/adminhtml_sizes_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('wheelsfinder')->__('Size does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
				
	  			
			$model = Mage::getModel('wheelsfinder/sizes');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$new_size = $model->save();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('wheelsfinder')->__('Size was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('wheelsfinder')->__('Unable to find size to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				
				$model = Mage::getModel('wheelsfinder/sizes');
				$vehicles_updated = $model->unsetVehiclesSizes($this->getRequest()->getParam('id'));
				
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				
				if(count($vehicles_updated['vehicles_ids_updates'])){
					
					if( Mage::helper('wheelsfinder')->getDebugMode() ){
					
						$vehicles_update_names = Mage::helper('wheelsfinder')->getVehicleNameById($vehicles_updated['vehicles_ids_updates']);
						Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Size was successfully deleted, vehicles width this size update:<br />').$vehicles_update_names);
						
					}else{
						
						Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Size was successfully deleted'));
						
					}

				}else{
				
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Size was successfully deleted'));
					
				}
				
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $sizesIds = $this->getRequest()->getParam('sizes');
        if(!is_array($sizesIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select size(s)'));
        } else {
            try {
				
				$message_update = '';
				$comma = '';
				$counter = 0;
				
                foreach ($sizesIds as $sizesId) {
					
					$size = Mage::getModel('wheelsfinder/sizes')->load($sizesId);
					$vehicles_updated = $size->unsetVehiclesSizes($sizesId);
					
					if($counter > 0){
						$comma = ', ';
					}
					
					if(count($vehicles_updated['vehicles_ids_updates'])){
						$message_update = $message_update.$comma.Mage::helper('wheelsfinder')->getVehicleNameById($vehicles_updated['vehicles_ids_updates']);
					}
					
                    $size->delete();
					
					$counter++;
                }
				
				if( Mage::helper('wheelsfinder')->getDebugMode() ){
					
					if($message_update){
						
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('wheelsfinder')->__(
								'Total of %d record(s) were successfully deleted <br />Vehicles update: ', count($sizesIds)
							).$message_update
						);
						
					}else{
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('wheelsfinder')->__(
								'Total of %d record(s) were successfully deleted ', count($sizesIds)
							)
						);
					}
				}else{
					
					Mage::getSingleton('adminhtml/session')->addSuccess(
						Mage::helper('wheelsfinder')->__(
							'Total of %d record(s) were successfully deleted ', count($sizesIds)
						)
					);
					
				}
				
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed()
    {
        return true;
    }
}