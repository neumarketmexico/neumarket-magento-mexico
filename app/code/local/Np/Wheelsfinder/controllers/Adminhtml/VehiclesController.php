<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials controller
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_Adminhtml_VehiclesController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('vehicles/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Manager Vehicles'), Mage::helper('adminhtml')->__('Manager Vehicles'))
			->getLayout()->getBlock('head')->setTitle($this->__('Manager Vehicles'));
		
		return $this;
	}
	
	public function indexAction() {
		$this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('wheelsfinder/adminhtml_vehicles'));
        $this->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('wheelsfinder/vehicles')->load($id);
		
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			
			if (!empty($data)) {
				$model->setData($data);
			}
			
			Mage::register('vehicles_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('vehicles/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Vehicles Manager'), Mage::helper('adminhtml')->__('Vehicle Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Vehicles News'), Mage::helper('adminhtml')->__('Vehicles News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('wheelsfinder/adminhtml_vehicles_edit'))
				->_addLeft($this->getLayout()->createBlock('wheelsfinder/adminhtml_vehicles_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('wheelsfinder')->__('Vehicle does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		
		if ($data = $this->getRequest()->getPost()) {

            if(isset($_FILES['img']['name']) and (file_exists($_FILES['img']['tmp_name']))) {
                try {
                    $uploader = new Varien_File_Uploader('img');
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); // or pdf or anything
                    $uploader->setAllowRenameFiles(true); //Modifica el nombre del archivo si ya existe
                    $uploader->setFilesDispersion(false); // setFilesDispersion(true) -> move your file in a folder the magento way
                    $path = Mage::getBaseDir('media') . DS .'vehicles/original'. DS ;

                    //
                    //Probar renombrando el archivo con la marca-modelo-linea del auto
                    //

                    //$uploader->save($path, $_FILES['img']['name']); Guarda con el nombre original de la imagen
                    $vehicle_name = $data['marca'].'-'.$data['modelo'].'-'.$data['linea'];
                    $uploader->save($path, $vehicle_name.'.'.$uploader->getFileExtension()); //Guarda con el nombre del vehículo y la extensión de la imagen subida
                    $new_name = $uploader->getUploadedFileName(); //Devuelve el nombre del archivo subido, por si lo tuvo que modificar
                    $data['img'] = 'vehicles/'.$new_name;
                    $data['hasimg'] = 'Yes';

                    $path = Mage::getBaseDir('media') . DS .'vehicles'. DS ;
                    $imageObj = new Varien_Image(Mage::getBaseDir('media') . DS .'vehicles/original'. DS . $new_name);
                    $imageObj->keepAspectRatio(TRUE);
                    $imageObj->keepAspectRatio(TRUE);
                    $imageObj->keepFrame(TRUE);
                    $imageObj->backgroundColor(array(255,255,255));
                    $imageObj->resize(500, 300);
                    $imageObj->save($path,$new_name);

                } catch(Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError('Problema subiendo imagen');
                }
            } else {
                if(isset($data['img']['delete']) && $data['img']['delete'] == 1) {
                    unlink(Mage::getBaseDir('media') . DS . $data['img']['value']);
                    $data['img'] = '';
                    $data['hasimg'] = 'No';
                } else {
                    unset($data['img']);
                }
            }

			$model = Mage::getModel('wheelsfinder/vehicles');	
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			$vehicle_sizes_ids = array();
			$vehicle_size_ids_saved = NULL;
			$vehicle_size_ids_removed = NULL;

			if (isset($data['vehicle_create_size'])) {

				$ids_saved = $model->saveSizesCreatedByVehicle($data['vehicle_create_size']);
				
				if(count($ids_saved)){
					
					if(!isset($data['vehicle_size'])){
						$data['vehicle_size'] = array();
					}
					
					$array_vehicles_sizes = $data['vehicle_size'];
					
					foreach($ids_saved as $id){
						$array_vehicles_sizes[] = array('is_delete'=>'', 'size_id' => $id);
					}
					$data['vehicle_size'] = $array_vehicles_sizes;
				}
				
				unset($data['vehicle_create_size']);

			}
			
			if(isset($data['vehicle_size'])){
				
				$vehicle_sizes = $data['vehicle_size'];
				
				if( count($vehicle_sizes) ){
			
					foreach($data['vehicle_size'] as $key => $val){
						
						if($val['is_delete'] == 1){
							unset($data['vehicle_size'][$key]);
						}else if(!$val['size_id']){
							unset($data['vehicle_size'][$key]);
						}else if($val['size_id'] && !$val['is_delete']){
							$vehicle_sizes_ids[] = $val['size_id'];
						}
						
					}
					
				}
				
			}
			
			if( $this->getRequest()->getParam('id') ){
				if($vehicle_sizes_ids){
					//Sizes saved
					$vehicle_size_ids_saved = $model->setVehicleSize( $this->getRequest()->getParam('id'), $vehicle_sizes_ids );
				}else if(!count($vehicle_sizes_ids)){
					//Sizes removed
					$vehicle_size_ids_removed = $model->removeVehicleSizes($this->getRequest()->getParam('id'));
				}
			}
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$new_vehicle = $model->save();
				
				if( !$this->getRequest()->getParam('id') ){
					if($vehicle_sizes_ids){
						//Sizes saved
						$vehicle_size_ids_saved = $model->setVehicleSize($new_vehicle->getId(), $vehicle_sizes_ids);
					}else if(!count($vehicle_sizes_ids)){
						//Sizes removed
						$vehicle_size_ids_removed = $model->removeVehicleSizes($new_vehicle->getId());
					}
				}
				
				$saving_errors = NULL;
				$saving_success = NULL;
				
				if(isset($vehicle_size_ids_saved['errors'])) {
					$saving_errors = $vehicle_size_ids_saved['errors'];
				}
				
				if(isset($vehicle_size_ids_saved['saved'])) {
					$saving_success = $vehicle_size_ids_saved['saved'];
				}

				if($saving_errors || $saving_success){
				
					if(count($saving_errors)){
						
						if( Mage::helper('wheelsfinder')->getDebugMode() ){
							$errors_sizes = Mage::helper('wheelsfinder')->formatSavedSizes($saving_errors);
							Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('wheelsfinder')->__('Vehicle was successfully saved but some sizes has errors, please try again. <br />Error sizes: '.$error_sizes));
						}else{
							Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('wheelsfinder')->__('Vehicle was successfully saved but some sizes has errors, please try again.'));
						}

					}else if(count($saving_success)){
						
						if( Mage::helper('wheelsfinder')->getDebugMode() ){
							$saved_sizes = Mage::helper('wheelsfinder')->formatSavedSizes($saving_success);
							Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('wheelsfinder')->__('Vehicle was successfully saved. <br />Sizes saved: ').$saved_sizes);
						}else{
							Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('wheelsfinder')->__('Vehicle was successfully saved.'));
						}

					}
					
				}else{
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('wheelsfinder')->__('Vehicle was successfully saved'));
				}
				
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('wheelsfinder')->__('Unable to find size to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('wheelsfinder/vehicles');
				
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Vehicle was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $vehiclesIds = $this->getRequest()->getParam('vehicles');
        if(!is_array($vehiclesIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select vehicle(s)'));
        } else {
            try {
                foreach ($vehiclesIds as $vehiclesId) {
					
					$vehicle = Mage::getModel('wheelsfinder/vehicles')->load($vehiclesId);
                    $vehicle->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted ', count($vehiclesIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed()
    {
        return true;
    }

}