<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials controller
 *
 * @category   Nicolas
 * @package    Nicolas_Testimonials
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_sizesController extends Mage_Core_Controller_Front_Action
{
	
	protected function _getSizesSession()
	{
		return Mage::getSingleton('core/session');
	}
    
	public function indexAction()
    {
		$url = Mage::app()->getStore()->getBaseUrl();
		Mage::app()->getResponse()->setRedirect( $url );
    }
	
	public function recoveryAction()
    {
		$data = $this->getRequest()->getPost();

		$ancho = NULL;
		$perfil = NULL;
		$rin = NULL;
		
		if( isset($data['wf-anchos']) and !empty($data['wf-anchos']) ){
			$ancho = $data['wf-anchos'];
		}
		
		if( isset($data['wf-perfiles']) and !empty($data['wf-perfiles']) ){
			$perfil = $data['wf-perfiles'];
		}
		
		if($ancho && !$perfil && !$rin){
			
			$this->_getSizesSession()->setData('wf_ancho', $ancho);
			Mage::registry('wf_ancho', $ancho);
			
			$perfiles = Mage::getModel('wheelsfinder/sizes')->getPerfilesByAncho($ancho);
			
			$response = new Varien_Object();
			$response->setData('data_perfiles', $perfiles);
			
			return $this->getResponse()->setBody( Mage::helper('core')->jsonEncode($response) );
			
		}
		
		if(!$ancho && $perfil && !$rin){
			
			$this->_getSizesSession()->setData('wf_perfil', $perfil);
			Mage::registry('wf_perfil', $perfil);
			
			$rins = Mage::getModel('wheelsfinder/sizes')->getRinsByAnchoAndPerfil($this->_getSizesSession()->getData('wf_ancho'), $perfil);
				
			$response = new Varien_Object();
			$response->setData('data_rins',$rins);
			
			return $this->getResponse()->setBody( Mage::helper('core')->jsonEncode($response) );
			
		}
		
    }

    /**
     *
     */
    public function resultAction()
    {	 
		$aliases = $this->getRequest()->getAliases();
		$redirect_rewrite = NULL;
		
		if(!count($aliases) && !isset($aliases['rewrite_request_path']) ){
			$redirect_rewrite = true;
		}
		
		$data = $this->getRequest()->getParams();
		$session = $this->_getSizesSession();
		
		$ancho = NULL;
		$perfil = NULL;
		$rin = NULL;
		$title = NULL;
		$criteria = array();
		
		if( isset($data['wf-anchos']) and !empty($data['wf-anchos']) ){
			$ancho = $data['wf-anchos'];
			$session->setData('wf_ancho', $ancho);
			
			Mage::registry('wf_ancho', $ancho);
			
			$title = $title.$ancho;
			$criteria['Ancho'] = $ancho;
		}
		
		if( isset($data['wf-perfiles']) and !empty($data['wf-perfiles']) ){
			$perfil = $data['wf-perfiles'];
			$session->setData('wf_perfile', $perfil);
			
			Mage::registry('wf_perfile', $perfil);
			
			$title = $title.' '.$perfil;
			$criteria['Perfil'] = $perfil;
		}
		
		if( isset($data['wf-rins']) and !empty($data['wf-rins']) ){
			$rin = $data['wf-rins'];
			$session->setData('wf_rin', $rin);
			
			Mage::registry('wf_rin', $rin);
			
			$title = $title.' R'.$rin.' | Neumarket México';
			$criteria['Rin'] = $rin;
		}
		
		//var_dump($perfil);
		//exit();
		
		$size_id = Mage::getModel('wheelsfinder/sizes')->getSizeIdByFields($ancho, $perfil, $rin);
		
		if($redirect_rewrite){
			$size = Mage::getModel('wheelsfinder/sizes')->load($size_id);
			if($size->getUrlRewrite()){
                /* Modificación para formulario Dunlop */
                if(isset($data['fabricante'])){
                    if($data['fabricante']=='dunlop'){
                        Mage::app()->getResponse()->setRedirect($size->getUrlRewrite().'?brands=50');
                    }elseif($data['fabricante']=='toyo'){
                        Mage::app()->getResponse()->setRedirect($size->getUrlRewrite().'?brands=250');
                    }

                }
                else {
                    Mage::app()->getResponse()->setRedirect($size->getUrlRewrite());
                }
			}
		}
		
		$session
			->setData('size_results', $size_id)
			->setData('criteria', $criteria);
			//->addSuccess($this->__('Resultados de busqueda por medidas: ').$title."''");

        //Valores a usar para Meta Description
        $product_collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect(array('name', 'price', 'visibility', 'brands'))
            ->addAttributeToSort('price')
            ->addFieldToFilter('wheelsfinder_sizes', $size_id);
        $cant = $product_collection->count();

        $i = 0;
        $brands = array();
        $min_price = "";
        foreach($product_collection as $prod){
            if($i==0){
                $min_price = Mage::helper('core')->currency($prod->getFinalPrice(), true, false);
            }
            $i++;
            $brands[] = $prod->getAttributeText('brands');
        }
        $brands = array_values(array_unique($brands));

        $cant_brands = count($brands);
        $cant_brands = min($cant,3);
        $brands_string = '';
        $i = 0;
        for ($i = 0; $i < $cant_brands; $i++){
            if($i==0){
                if(isset($brands[$i])) {
                    $brands_string = $brands[$i];
                }
            } else if($i==$cant_brands-1) {
                if(isset($brands[$i])) {
                    $brands_string = $brands_string . ' y ' . $brands[$i];
                }
            } else {
                if(isset($brands[$i])) {
                    $brands_string = $brands_string . ', ' . $brands[$i];
                }
            }
        }

		$this->loadLayout()
			->getLayout()->getBlock('head')
			->setDescription($this->__('Llantas ').$criteria['Ancho'].'/'.$criteria['Perfil'].' R'.$criteria['Rin'].' en '.$cant.' referencias disponibles, desde '.$min_price.' c/u, en marcas como '.$brands_string.'. Llama sin costo al 01800 8906213 desde cualquier lugar de la República Mexicana.')
			->setKeywords($this->__('llantas ').$title.(' neumarket colombia bogota'))
			->setTitle($this->__('Llantas ').$title);
	//"Llantas 195/55 R15 en 23 referencias disponibles, desde $154.900 c/u, en marcas como Hankook, Linglong y Michelin. Llama al 7435595 en Bogotá o al 018000 185595 en el resto del país"


        //die();
		$this->renderLayout();
		
    }
}