<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();

$conn = $installer->getConnection();
$table = $installer->getTable('wheelsfinder_vehicles');

$conn->addColumn($table, 'mensaje', 'varchar(255)');

$installer->endSetup(); 