<?php
/**
 * Ezequiel Klusman
 */
$installer = $this;
$installer->startSetup();

$conn = $installer->getConnection();
$table = $installer->getTable('wheelsfinder_vehicles');

$conn->addColumn($table, 'img', 'varchar(255)');

$installer->endSetup(); 