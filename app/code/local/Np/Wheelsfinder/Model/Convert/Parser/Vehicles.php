<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Np_Wheelsfinder_Model_Convert_Parser_Vehicles extends Mage_Dataflow_Model_Convert_Parser_Abstract
{
	/**
     * Vehicles model
     *
     * @var Np_Wheelsfinder_Model_Vehicles
     */
    protected $_vehiclesModel;
	
	/**
     * Sizes collection
     *
     */
    protected $_collections;
	
    /**
     * Retrieve sizes model cache
     *
     * @return Np_Wheelsfinder_Model_Sizes
     */
    public function getVehiclesModel()
    {
        if (is_null($this->_vehiclesModel)) {
            $object = Mage::getModel('wheelsfinder/vehicles');
            $this->_vehiclesModel = Mage::objects()->save($object);
        }
        return Mage::objects()->load($this->_vehiclesModel);
    }
	
	public function getCollection()
    {
        if (!isset($this->_collections)) {
            $this->_collections = Mage::getResourceModel('wheelsfinder/vehicles_collection');
        }
        return $this->_collections;
    }
	
    public function unparse()
    {
		$collection = $this->getCollection();
		$i = 0;
		//$entityIds = $this->getData();
		
        foreach ($collection as $vehicle) {
            
			//$size = $this->getSizesModel()
            //    ->setData(array())
            //    ->load($entityId);
            /* @var $size Np_Wheelsfinder_Model_Sizes */
			
            $position = Mage::helper('wheelsfinder')->__('Line %d, Size: %s', ($i+1), ($vehicle->getMarca().' / '.$vehicle->getModelo().' '.$vehicle->getLinea()) );
            $this->setPosition($position);

            $row = array();

            foreach ($vehicle->getData() as $field => $value) {
                $row[$field] = $value;
				unset($row['created_time']);
				unset($row['update_time']);
            }
			
			$size_ids = $vehicle->getVehiclesSizesByVechicleId( $vehicle->getId() );
			
			if( count($size_ids) ){
				$row['size_ids'] = implode(',', $size_ids);
			}
			
			//print_r($row);
			
			
            $batchExport = $this->getBatchExportModel()
                ->setId(null)
                ->setBatchId($this->getBatchModel()->getId())
                ->setBatchData($row)
                ->setStatus(1)
                ->save();
				//print_r($batchExport);
			//exit();
		$i++;
        }

        return $this;
    }
	
	public function parse()
    {
	}
}
