<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Np_Wheelsfinder_Model_Convert_Adapter_Sizes extends Mage_Dataflow_Model_Convert_Adapter_Abstract
{
	/**
     * Sizes model
     *
     * @var Np_Wheelsfinder_Model_Sizes
     */
    protected $_sizesModel;
	
	protected $_requiredFields = array();
	
	public function __construct()
	{
		$this->_requiredFields[] = 'ancho';
		$this->_requiredFields[] = 'perfil';
		$this->_requiredFields[] = 'rin';
	}
	
    /**
     * Retrieve sizes model cache
     *
     * @return Np_Wheelsfinder_Model_Sizes
     */
    public function getSizesModel()
    {
        if (is_null($this->_sizesModel)) {
            $object = Mage::getModel('wheelsfinder/sizes');
            $this->_sizesModel = Mage::objects()->save($object);
        }
        return Mage::objects()->load($this->_sizesModel);
    }

    public function load()
    {
    }

    public function save()
    {
    }

    /*
     * saveRow function for saving each size data
     *
     * params args array
     * return array
     */
    public function saveRow($importData)
    {
		
		$size = $this->getSizesModel();
        //$size->setId(null);
		$size_id = NULL;
		
		foreach ($this->_requiredFields as $field) {
			if (!isset($importData[$field])) {
				$message = Mage::helper('wheelsfinder')->__('Skipping import row, required field "%s" is not defined.', $field);
				Mage::throwException($message);
			}
		}
		
		if( isset($importData['sizes_id']) && !empty($importData['sizes_id']) ) {
			$size_id = $importData['sizes_id'];
			unset( $importData['sizes_id'] );
		} elseif( isset($importData['sizes_id']) && empty($importData['sizes_id']) ) {
			unset( $importData['sizes_id'] );
		}
		
		unset( $importData['sizes_id'] );
		
        if ( $size_id ) {
            $size->load($size_id);
        }
		
		if( !$size->getId() ) {
			$size_id = $size->getSizeIdByFields($importData['ancho'], $importData['perfil'], $importData['rin']);
			$size->load($size_id);
		}
		
		foreach ($importData as $field => $value) {
            $size->setData($field, $value);
        }
		
		if ( $size->getCreatedTime() && !$size->getUpdateTime() ) {
			
			$size->setUpdateTime(now());
			
		} elseif( !$size->getCreatedTime() && !$size->getUpdateTime() ) {
			
			$size->setCreatedTime(now())->setUpdateTime(now());
			
		} elseif( !$size->getCreatedTime() && $size->getUpdateTime() ) {
			
			$size->setCreatedTime(now())->setUpdateTime(now());
			
		} elseif( $size->getCreatedTime() && $size->getUpdateTime() ) {
			
			$size->setUpdateTime(now());
			
		}
		
        //$size->setImportMode(true);
		$size->save();
		
		
        return $this;
    }
}
