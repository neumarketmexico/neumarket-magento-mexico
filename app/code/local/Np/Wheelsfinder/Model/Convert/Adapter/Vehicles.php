<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Np_Wheelsfinder_Model_Convert_Adapter_Vehicles extends Mage_Dataflow_Model_Convert_Adapter_Abstract
{
	/**
     * Vehicles model
     *
     * @var Np_Wheelsfinder_Model_Vehicles
     */
    protected $_vehiclesModel;
	
	protected $_requiredFields = array();
	
	public function __construct()
	{
		$this->_requiredFields[] = 'marca';
		$this->_requiredFields[] = 'modelo';
		$this->_requiredFields[] = 'linea';
	}
	
    /**
     * Retrieve sizes model cache
     *
     * @return Np_Wheelsfinder_Model_Sizes
     */
    public function getVehiclesModel()
    {
        if (is_null($this->_vehiclesModel)) {
            $object = Mage::getModel('wheelsfinder/vehicles');
            $this->_vehiclesModel = Mage::objects()->save($object);
        }
        return Mage::objects()->load($this->_vehiclesModel);
    }

    public function load()
    {
    }

    public function save()
    {
    }

    /*
     * saveRow function for saving each size data
     *
     * params args array
     * return array
     */
    public function saveRow($importData)
    {
		
		$vehicle = $this->getVehiclesModel();
		$vehicle_id = NULL;
		$size_ids = NULL;
		
		foreach ($this->_requiredFields as $field) {
			if (!isset($importData[$field])) {
				$message = Mage::helper('wheelsfinder')->__('Skipping import row, required field "%s" is not defined.', $field);
				Mage::throwException($message);
			}
		}
		
		if( isset($importData['vehicles_id']) && !empty($importData['vehicles_id']) ) {
			$vehicle_id = $importData['vehicles_id'];
			unset( $importData['vehicles_id'] );
		} elseif( isset($importData['vehicles_id']) && empty($importData['vehicles_id']) ) {
			unset( $importData['vehicles_id'] );
		}
		
		unset( $importData['vehicles_id'] );
		
		if( isset($importData['size_ids']) ) {
			$size_ids = $importData['size_ids'];
			unset($importData['size_ids']);
		}
		
        if ( $vehicle_id ) {
            $vehicle->load($vehicle_id);
        }
		
		if( !$vehicle->getId() ) {
			$vehicle_id = $vehicle->getVehicleIdByFields($importData['marca'], $importData['modelo'], $importData['linea']);
			$vehicle->load($vehicle_id);
		}
		
		foreach ($importData as $field => $value) {
            $vehicle->setData($field, $value);
        }
		
		if ( $vehicle->getCreatedTime() && !$vehicle->getUpdateTime() ) {
			
			$vehicle->setUpdateTime(now());
			
		} elseif( !$vehicle->getCreatedTime() && !$vehicle->getUpdateTime() ) {
			
			$vehicle->setCreatedTime(now())->setUpdateTime(now());
			
		} elseif( !$vehicle->getCreatedTime() && $vehicle->getUpdateTime() ) {
			
			$vehicle->setCreatedTime(now())->setUpdateTime(now());
			
		} elseif( $vehicle->getCreatedTime() && $vehicle->getUpdateTime() ) {
			
			$vehicle->setUpdateTime(now());
			
		}
		
        //$size->setImportMode(true);
		$vehicle->save();
		
		if($size_ids){
			$size_ids = explode(',', $size_ids);
			$vehicle_id = $vehicle->getId();
			$vehicle->setVehicleSize($vehicle_id, $size_ids);
		
		}
		
        return $this;
    }
}
