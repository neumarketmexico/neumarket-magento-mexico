<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials
 *
 * @category   Nicolas
 * @package    Nicolas_Wheelsfinder
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup
{
	public function getDefaultEntities()
    {
        return array(
            'catalog_product' 				=> array(
                'entity_model'      			=> 'catalog/product',
                'attribute_model'   			=> 'catalog/resource_eav_attribute',
                'table'             			=> 'catalog/product',
				'additional_attribute_table'    => 'catalog/eav_attribute',
                'entity_attribute_collection'   => 'catalog/product_attribute_collection',
                'attributes'        			=> array(
                    'wheelsfinder_sizes' 			=> array(
						'type'              			=> 'int',
                        'group'             			=> 'General',
                        'label'             			=> 'Medidas',
                        'input'             			=> 'select',
                        'default'           			=> '',
                        'class'             			=> '',
                        'backend'           			=> '',
                        'frontend'          			=> '',
                        'source'            			=> 'wheelsfinder/resource_eav_source',
                        'global'            			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'visible'           			=> true,
                        'required'          			=> false,
                        'user_defined'      			=> false,
                        'searchable'        			=> true,
                        'filterable'        			=> true,
                        'comparable'        			=> true,
                        'visible_on_front'  			=> true,
                        'visible_in_advanced_search' 	=> true,
                        'unique'            			=> false
                    ),
                )
            )
		);
	}
}