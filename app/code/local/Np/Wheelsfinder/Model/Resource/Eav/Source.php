<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials
 *
 * @category   Nicolas
 * @package    Nicolas_Wheelsfinder
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Wheelsfinder_Model_Resource_Eav_Source extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	public function getAllOptions()
    {
		if (!$this->_options) {
			
			$options_sizes = Mage::getModel('wheelsfinder/sizes')->getSizesOptionsArray();
			$this->_options = array();
			$this->_options[] = array('value' => '', 'label' => '');
			
			foreach($options_sizes as $id => $txt):
			
				$this->_options[] = array('value' => $id, 'label' => $txt);
			
			endforeach;
		
        }
        return $this->_options;
    }
}