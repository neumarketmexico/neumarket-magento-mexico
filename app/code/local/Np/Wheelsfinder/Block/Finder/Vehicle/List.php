<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Np_Wheelsfinder_Block_Finder_Vehicle_List extends Mage_Catalog_Block_Product_List
{
	
	protected function _getVehiclesSession()
	{
		return Mage::getSingleton('core/session');
	}
	
	protected function _getProductCollection()
    {

        /*$session = $this->_getVehiclesSession();
		$sizes_ids = $session->getData('size_results');
		
		if( count($sizes_ids) ){
			
			$product_collection = Mage::getModel('catalog/product')
				->getCollection()
				->addAttributeToSelect('*')
				->addFieldToFilter('wheelsfinder_sizes', $sizes_ids);
			
			$this->_productCollection = $product_collection;
			
		}else{
		
			if (is_null($this->_productCollection)) {
				
				$layer = $this->getLayer();
				$this->_productCollection = $layer->getProductCollection();
				
			}
		
		}

        return $this->_productCollection;*/
		return parent::_getProductCollection();
		
    }
	
	public function getSearchName()
	{
		return 'Veh&iacute;culo';
	}
	
	public function getSearchCriteria()
	{
		$session = $this->_getVehiclesSession();
		return $session->getData('criteria');
	}
	
	public function getSearchMessage()
	{
		$session = $this->_getVehiclesSession();
		return $session->getData('wf-message');
	}

}