<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Np_Wheelsfinder_Block_Finder_Vehicle_Form extends Mage_Core_Block_Template
{
	protected function _getVehiclesSession()
	{
		return Mage::getSingleton('core/session');
	}
	
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
	
	/**
     * Retrieve wheelsfinder vehicles model object
     *
     * @return Np_Wheelsfinder_Model_Vehicles
     */
    public function getModel()
    {
        return Mage::getSingleton('wheelsfinder/vehicles');
    }
	
	public function getSearchPostUrl()
    {
        return $this->getUrl('wheelsfinder/vehicles/result', array('_secure'=>true));
    }
	
	public function getRecoveryUrl()
	{
		return $this->getUrl('wheelsfinder/vehicles/recovery', array('_secure'=>true));
	}
	
	public function getAvailableMarcas()
	{
	    /*
		$collection = $this->getModel()
			->getCollection()
			->setOrder('marca', 'asc')
			->getColumnValues('marca');
*/
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT `marca` FROM `wheelsfinder_vehicles` GROUP BY `marca` ORDER BY `marca` ASC";
        return $collection       = $connection->fetchCol($sql);
        //Zend_Debug::dump($collection);
/*
		$availables = array();
		
		foreach($collection as $marca){
			if( !in_array($marca, $availables) ){
				$availables[] = $marca;
			}
		}
		
		return $availables;
*/
	}
	
	public function getCurrentMarcaAndModelos()
	{
		
		$session = $this->_getVehiclesSession();
		
		$marca = NULL;
		$modelos = NULL;
		
		if($session->getData('wf_marca')){
			$marca = $session->getData('wf_marca');
			$modelos = Mage::getModel('wheelsfinder/vehicles')->getModelosByMarca($marca);
		}
		
		if( Mage::registry('wf_marca') ){
			$marca = Mage::registry('wf_marca');
			$modelos = Mage::getModel('wheelsfinder/vehicles')->getModelosByMarca($marca);
		}

		$return = array('wf-marca-selected'=>$marca, 'wf-modelos'=>$modelos);

		return $return;
	}
	
	public function getCurrentModeloAndLineas()
	{
		$session = $this->_getVehiclesSession();
		$marca = NULL;
		$modelo = NULL;
		$lineas = NULL;
		
		if($session->getData('wf_marca') && $session->getData('wf_modelo')){
			$marca = $session->getData('wf_marca');
			$modelo = $session->getData('wf_modelo');
			$lineas = Mage::getModel('wheelsfinder/vehicles')->getLineasByMarcaAndModelo($marca, $modelo);
		}
		
		$return = array('wf-modelo-selected'=>$modelo, 'wf-lineas'=>$lineas);
		
		return $return;
	}
	
	public function getCurrentLinea()
	{
		$session = $this->_getVehiclesSession();
		
		$linea = NULL;
		
		if( $session->getData('wf_linea') ){
			$linea = $session->getData('wf_linea');
		}
		
		$return = array('wf-linea-selected'=>$linea);
		
		return $return;
	}

}