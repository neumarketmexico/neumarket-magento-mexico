<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Wheelsfinder_Block_Adminhtml_Vehicles_Edit_Tab_Sizes extends Mage_Adminhtml_Block_Widget_Form
{
	public function getVehiclesId()
	{
		$id = NULL;
		
		if ( Mage::getSingleton('adminhtml/session')->getVehiclesData() ){
			$data = Mage::getSingleton('adminhtml/session')->getVehiclesData();
			$id = $data['vehicles_id'];
		} elseif ( Mage::registry('vehicles_data') ) {
			$data = Mage::registry('vehicles_data')->getData();
			if( count($data) && isset($data['vehicles_id']) ){
				$id = $data['vehicles_id'];
			}
		}
		
		return $id;
	}
	
	public function getVehiclesSizesByVechicleId($id)
	{
		return Mage::getModel('wheelsfinder/vehicles')->getVehiclesSizesByVechicleId($id);
	}
}