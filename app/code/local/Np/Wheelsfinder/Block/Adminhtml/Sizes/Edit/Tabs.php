<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Wheelsfinder_Block_Adminhtml_Sizes_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('sizes_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('wheelsfinder')->__('Size Information'));
  }
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('wheelsfinder')->__('Size Information'),
          'title'     => Mage::helper('wheelsfinder')->__('Size Information'),
          'content'   => $this->getLayout()->createBlock('wheelsfinder/adminhtml_sizes_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}