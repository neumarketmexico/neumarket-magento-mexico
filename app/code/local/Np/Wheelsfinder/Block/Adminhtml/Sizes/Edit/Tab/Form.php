<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Wheelsfinder_Block_Adminhtml_Sizes_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('sizes_form', array('legend'=>Mage::helper('wheelsfinder')->__('Size information')));
     
      $fieldset->addField('ancho', 'text', array(
          'label'     => Mage::helper('wheelsfinder')->__('Ancho'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'ancho',
      ));
	  
	  $fieldset->addField('perfil', 'text', array(
          'label'     => Mage::helper('wheelsfinder')->__('Perfil'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'perfil',
      ));
	  
	  $fieldset->addField('rin', 'text', array(
          'label'     => Mage::helper('wheelsfinder')->__('Rin'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'rin',
      ));
	  
	  
      if ( Mage::getSingleton('adminhtml/session')->getSizesData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getSizesData());
          Mage::getSingleton('adminhtml/session')->setSizesData(null);
      } elseif ( Mage::registry('sizes_data') ) {
          $form->setValues(Mage::registry('sizes_data')->getData());
      }
      return parent::_prepareForm();
  }
}