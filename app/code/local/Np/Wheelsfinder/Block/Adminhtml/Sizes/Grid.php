<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Wheelsfinder_Block_Adminhtml_Sizes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('sizesGrid');
      $this->setDefaultSort('sizes_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('wheelsfinder/sizes')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('sizes_id', array(
          'header'    => Mage::helper('wheelsfinder')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'sizes_id',
      ));

      $this->addColumn('ancho', array(
          'header'    => Mage::helper('wheelsfinder')->__('Ancho'),
          'align'     =>'left',
          'index'     => 'ancho',
      ));
	  
	  $this->addColumn('perfil', array(
          'header'    => Mage::helper('wheelsfinder')->__('Perfil'),
          'align'     =>'left',
          'index'     => 'perfil',
      ));
	  
	  $this->addColumn('rin', array(
          'header'    => Mage::helper('wheelsfinder')->__('Rin'),
          'align'     =>'left',
          'index'     => 'rin',
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('wheelsfinder')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('wheelsfinder')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		//$this->addExportType('*/*/exportCsv', Mage::helper('wheelsfinder')->__('CSV'));
		//$this->addExportType('*/*/exportXml', Mage::helper('wheelsfinder')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('sizes_id');
        $this->getMassactionBlock()->setFormFieldName('sizes');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('wheelsfinder')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('wheelsfinder')->__('Are you sure?')
        ));
		
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}