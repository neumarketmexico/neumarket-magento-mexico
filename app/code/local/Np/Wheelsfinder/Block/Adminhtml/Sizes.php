<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Wheelsfinder_Block_Adminhtml_Sizes extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_sizes';
    $this->_blockGroup = 'wheelsfinder';
    $this->_headerText = Mage::helper('wheelsfinder')->__('Manage Sizes');
    $this->_addButtonLabel = Mage::helper('wheelsfinder')->__('Add Size');
    parent::__construct();
  }
}