<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Np_Brands_Block_Adminhtml_Brands_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('brands_form', array('legend'=>Mage::helper('brands')->__('Brand information')));
     
      $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('brands')->__('Status'),
            'title'     => Mage::helper('brands')->__('Page Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array( '1' => Mage::helper('brands')->__('Enabled'), '0' => Mage::helper('brands')->__('Disabled'), ),
        ));
	  
	  $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('brands')->__('Name'),
		  'title' 	  => Mage::helper('brands')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));
	  
	  $fieldset->addField('image', 'image', array(
          'label'     => Mage::helper('brands')->__('Logo'),
		  'title' 	  => Mage::helper('brands')->__('Logo'),
          'required'  => true,
          'name'      => 'image',
	  ));
	  
	  $fieldset->addField('identifier', 'text', array(
		  'name'      => 'identifier',
		  'label'     => Mage::helper('brands')->__('URL Key'),
		  'title'     => Mage::helper('brands')->__('URL Key'),
		  'required'  => true,
		  'class'     => 'validate-identifier',
		  'note'      => Mage::helper('brands')->__('Relative to Website Base URL'),
	  ));
	  
	  $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('brands')->__('Title'),
		  'title' 	  => Mage::helper('brands')->__('Title'),
          'name'      => 'title',
      ));
	  
	  $fieldset->addField('meta_keywords', 'textarea', array(
		  'name' => 'meta_keywords',
		  'label' => Mage::helper('brands')->__('Keywords'),
		  'title' => Mage::helper('brands')->__('Meta Keywords'),
	  ));

	  $fieldset->addField('meta_description', 'textarea', array(
		  'name' => 'meta_description',
		  'label' => Mage::helper('brands')->__('Description'),
		  'title' => Mage::helper('brands')->__('Meta Description'),
	  ));
	  
      if ( Mage::getSingleton('adminhtml/session')->getBrandsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getBrandsData());
          Mage::getSingleton('adminhtml/session')->setBrandsData(null);
      } elseif ( Mage::registry('brands_data') ) {
          $form->setValues(Mage::registry('brands_data')->getData());
      }
      return parent::_prepareForm();
  }
}