<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials controller
 *
 * @category   No
 * @package    No_Serviteca
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Brands_indexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$url = Mage::app()->getStore()->getBaseUrl();
		Mage::app()->getResponse()->setRedirect( $url );
    }
	
	public function viewAction()
    {
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('brands/brand')->load($id);
		
		if( $model->getId() && $model->getData('is_active') ){
			
			Mage::getSingleton('core/session')->setData('brandid', $id);
			
			$this->loadLayout()
			->getLayout()->getBlock('head')
			->setDescription( $model->getData('meta_description') )
            ->setKeywords( $model->getData('meta_keywords') )
			->setTitle( $model->getData('title') );

			$this->renderLayout();
		}
		
    }
}