<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials
 *
 * @category   Nicolas
 * @package    Nicolas_Wheelsfinder
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Brands_Model_Resource_Eav_Mysql4_Setup extends Np_Brands_Model_Resource_Setup
{
}