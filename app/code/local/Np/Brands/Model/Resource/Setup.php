<?php
/**
 * Nicolas Pereyra
 * nico094@gmail.com
 * @category    Nicolas
 * @package     Nicolas_Testimonials
 * @copyright   Copyright (c) 2011 Nicolas Pereyra (http://www.magento.nicolaswebdesign.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Testimonials
 *
 * @category   Nicolas
 * @package    Nicolas_Wheelsfinder
 * @author     Nicolas Pereyra <nico094@gmail.com>
 */
class Np_Brands_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup
{
	public function getDefaultEntities()
    {
        return array(
            'catalog_product' 				=> array(
                'entity_model'      			=> 'catalog/product',
                'attribute_model'   			=> 'catalog/resource_eav_attribute',
                'table'             			=> 'catalog/product',
				'additional_attribute_table'    => 'catalog/eav_attribute',
                'entity_attribute_collection'   => 'catalog/product_attribute_collection',
                'attributes'        			=> array(
                    'brands' 			=> array(
						'type'              			=> 'int',
                        'group'             			=> 'General',
                        'label'             			=> 'Marcas',
                        'input'             			=> 'select',
                        'default'           			=> '',
                        'class'             			=> '',
                        'backend'           			=> '',
                        'frontend'          			=> '',
                        'source'            			=> 'brands/resource_eav_source',
                        'global'            			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'visible'           			=> true,
                        'required'          			=> false,
                        'user_defined'      			=> false,
                        'searchable'        			=> true,
                        'filterable'        			=> false,
                        'comparable'        			=> true,
                        'visible_on_front'  			=> true,
                        'visible_in_advanced_search' 	=> false,
                        'unique'            			=> false
                    ),
                )
            )
		);
	}
}