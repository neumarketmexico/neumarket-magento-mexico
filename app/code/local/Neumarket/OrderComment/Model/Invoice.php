<?php

require_once 'Mage/Sales/Model/Order/Invoice.php';
class Neumarket_OrderComment_Model_Invoice extends Mage_Sales_Model_Order_Invoice
{

    public function addComment($comment, $notify=false, $visibleOnFront=false)
    {
        if (!($comment instanceof Mage_Sales_Model_Order_Invoice_Comment)) {
            //getting username
            $firstname = '';
            $lastname = '';
            if(Mage::getSingleton('admin/session')){
                $user = Mage::getSingleton('admin/session');
                if($user->getUser()){
                    $firstname = $user->getUser()->getFirstname();
                    $lastname = $user->getUser()->getLastname();
                    //$username = $user->getUser()->getUsername();
                }
            }
            $comment = Mage::getModel('sales/order_invoice_comment')
                ->setComment($comment." (".$firstname." ".$lastname.")")
                ->setIsCustomerNotified($notify)
                ->setIsVisibleOnFront($visibleOnFront);
        }
        $comment->setInvoice($this)
            ->setStoreId($this->getStoreId())
            ->setParentId($this->getId());
        if (!$comment->getId()) {
            $this->getCommentsCollection()->addItem($comment);
        }
        $this->_hasDataChanges = true;
        return $this;
    }

}
