<?php

class Neumarket_ContactoB2B_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        //Get current layout state
        $this->loadLayout();

        $block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'neumarket.contacto_b2b',
            array(
                'template' => 'neumarket/contacto_b2b.phtml'
            )
        );

        $this->getLayout()->getBlock('content')->append($block);
        //$this->getLayout()->getBlock('right')->insert($block, 'catalog.compare.sidebar', true);

        $this->_initLayoutMessages('core/session');

        $this->renderLayout();
    }

    public function sendemailAction()
    {
        //Fetch submited params
        $params = $this->getRequest()->getParams();

        $body = 'Contacto:<br/>'.
        $params['name'].'<br/>'.$params['email'].'<br/>'.$params['tel'].'<br/>'.$params['comment']
        ;

        $mail = new Zend_Mail();
        $mail->setBodyHTML($body);
        $mail->setFrom($params['email'], $params['name']);
        $mail->addTo('ezeklusman@gmail.com', 'Some Recipient');
        $mail->setSubject('Test B2B Mail');
        try {
            $mail->send();
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email.');

        }

        $this->_redirect('contacto-b2b/');
    }
}

?>