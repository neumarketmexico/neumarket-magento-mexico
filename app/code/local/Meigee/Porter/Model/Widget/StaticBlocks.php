<?php
class Meigee_Porter_Model_Widget_StaticBlocks extends Meigee_Thememanager_Model_Widget_View_ThemeParametres
{
    const theme = 'Porter';



    public function toOptionArray()
    {
        return Mage::getModel('cms/block')->getCollection()->toOptionArray();
    }

}
