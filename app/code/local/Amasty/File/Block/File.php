<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_File
 */
class Amasty_File_Block_File extends Mage_Core_Block_Template 
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amfile/files.phtml');
    }

    protected function _toHtml()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        $productId = Mage::registry('current_product')->getId();

        $files = Mage::getResourceModel('amfile/file_collection')->getFilesFrontend($productId, $storeId);

        if (sizeof($files) == 0)
            return '';
        else
        {
            $this->setFiles($files);

            return parent::_toHtml();
        }
    }

    public function getBlockTitle()
    {
        if ($this->hasData('block_title')) {
            $title = $this->getData('block_title');
        }
        else {
            $title = Mage::getStoreConfig('amfile/block/title');
        }

        return $title;
    }
}
