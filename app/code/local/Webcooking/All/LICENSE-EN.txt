Vincent Enjalbert - Web Cooking
www.web-cooking.net

THIS LICENSE AGREEMENT (HEREINAFTER AGREEMENT) IS AN AGREEMENT BETWEEN YOU (THE
LICENSEE) AND Web Cooking (VINCENT ENJALBERT EI). THE AGREEMENT APPLIES TO ALL
PRODUCTS/SOFTWARE/SCRIPTS/SERVICES YOU PURCHASE FROM WEB COOKING.

1. By purchasing the Software you acknowledge that you have read this Agreement,
   and that you agree to the content of the Agreement and its terms, and agree
   to use the Software in compliance with this Agreement.

2. The Agreement comes into legal force after the delivery of the Software, which
   cannot be delivered before payment. This delivery is done by downloading, without
   any physical support. The licensee ask for this delivery to be made before the 
   expiration of the right of withdrawal, as stipulated in article L121-21 of the
   french consumer code, and the licensee states expressly waive his right of 
   withdrawal.

3. Web Cooking is the copyright holder of the Software. The Software or a 
   portion of it is a copyrightable matter and is liable to protection by the law. 
   Any activity that infringes terms of this Agreement violates copyright law and
   will be prosecuted according to the current law. We reserve the right to
   revoke the license of any user who is holding an invalid license.

4. This Agreement gives the licensee the right to use only one copy of the Software on
   one Magento installation solely. However, the licensee can use the Software on Magento
   instances that are not generating revenue and that are not accessible from public networks.
   A separate License should be purchased for each new Software installation. Any distribution 
   of the Software without our consent, including noncommercial distribution is regarded as
   violation of this Agreement and entails liability, according to the current law.

5. The licensee may not use any part of the code in whole or part in any other software
   or product or website.

6. The licensee may not give, sell, distribute, sub-license, rent, lease or lend any
   portion of the Software or Documentation to anyone. You may not place the
   Software on a server so that it is accessible via a public network such as
   the Internet for distribution purposes.

7. You are bound to preserve the copyright information intact, this includes the
   text/link at bottom of files of the Software.

8. This software is designed for virgin (= without any other extension installed 
   than those given by Magento) Magento COMMUNITY edition. Web Cooking does not 
   guarantee correct work of this extension on any other Magento edition except 
   Magento COMMUNITY edition. Web Cooking does not provide extension support in 
   case of incorrect edition usage.

9. Web Cooking will not be liable to you for any damages (including any loss of
   profits/saving, or incidental or consequential) caused to you, your
   information and your business arising out of the use or inability to use
   this Software.

10. Web Cooking is not liable for prosecution arising from use of the Software against
    law or for any illegal use.

11. The licensee may terminate it at any time by destroying all copies of the Software. 
    Termination of this Agreement does not bind Web Cooking to return the licensee 
    the amount spent for purchase of the Software.

12. Without prejudice to any damages Web Cooking could claim, Web Cooking retain 
    the right to terminate this license at any time, if in its sole discretion, 
    the licensee is not abiding by the terms of the Agreement, including, but not 
    limited to, obscuring or removing any link or copyright notice as specified 
    in this agreement. 
    The termination of this Agreement will be notified in writing. When the notification
    of termination is received by the licensee, the licensee will have to destroy all 
    copies of the Software.
    Web Cooking reservers the right to check, at the expense of the licensee, that 
    all copies have been destroyed.

13. If the licensee continue to use the Software after Web Cooking gives you notice
    of termination of your license, the licensee will be obligated to reimburse all 
    costs (including but not limited to attorney fees) exposed by Web Cooking in order 
    to assert legitimate its rights.

14. This license is subject to French law and only the courts of the spring
    of Paris jurisdiction in any dispute relating to this license.